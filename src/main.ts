



//  --- test interface ---
/*

import * as arsdk from '../arsdk/arsdk'

let print = arsdk.print
let nativeX = arsdk.nativeX

    // try {

        var startTime = new Date()

        print("calling nativeX.foo()")
        nativeX.foo()
        print("finished calling nativeX.foo()")

        print("1")
        print(arsdk.X.staticFoo())
        print(arsdk.X.staticWithParam(42))
        print("2")

        console.log("new X")
        var xx = new arsdk.X()
        console.log("X created")
        xx.foo()
        print("xx.i: ")
        print(xx.get_i())
        xx.set_i(1)
        print(xx.get_i())
        print("--")
        
        print("testing callback")
        xx.setCallback((i) => {
            print(`callback called ${i}`)
            // arsdk_print(xx.returnInt())
        })
        xx.callCallback()
        print("end testing callback")
        
        print("testing: call swift func that takes an optional")
        xx.takesOptional(42)
        xx.takesOptional(null)
        print("finished testing: call swift func that takes an optional")

        let loopCount = 100
        print(`starting loop with count ${loopCount}...`)
        let y = 2
        for (let i = 0; i < loopCount; ++i) {
            xx.fp2(i, y)
        }
        print("finished loop")
        
        var yy = new arsdk.Y(42)
        print(yy.i())
        yy.set_i(43)
        print(yy.i())
        yy.takesCodable({ "x": 1, "y": 2 })
        
        //setInterval(1000, () => { arsdk_print("tick") })
        print("done!")

        var endTime = new Date()
        print(endTime.getTime() - startTime.getTime())

        // return r



    // } catch(x) {
    //     print("exception:")
    //     print(x)
    // }

*/


//  --- ARSDK interface ---

import * as arsdk from '../arsdk/arsdk'
let print = arsdk.print

var tapCount = 0

try {
    const objectDefName = arsdk.experience.getObjectDefinitionNames()[0]
    var objectDef: arsdk.ObjectDefinition = arsdk.experience.getObjectDefinition(objectDefName)
    objectDef.whenInstanceCreated((object: arsdk.ObjectInstance) => {
        console.log("object created")
        object.whenPlaced(() => {
            console.log("object placed")
            object.setOpacity(0.5)
            object.playAnimation(object.getAnimationNames()[0])
        })
        object.whenTapped(() => { 
            console.log("object tapped")
            tapCount++
            const animNames = object.getAnimationNames()
            const animName = animNames[tapCount % animNames.length]
            console.log(animName)
            object.playAnimation(animName)
        })
    })
} catch(x) {
    print("exception:")
    print(x)
}
