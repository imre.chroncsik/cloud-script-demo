//
//  WeakRef.swift
//  script
//
//  Created by Imre Chroncsik on 10/5/20.
//  Copyright © 2020 Ryot. All rights reserved.
//

import Foundation

class WeakRef<T> where T: AnyObject {
    private(set) weak var value: T?
    
    init(value: T?) {
        self.value = value
    }
}

class FlexiRef<T> where T: AnyObject {
    private weak var value: T?
    private var strongValue: T?
    init(weak value: T?) { self.value = value }
    init(strong value: T?) { self.value = value; self.strongValue = value }
}
