//
//  ARTypeRegistry.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 6/2/20.
//  Copyright © 2020 RYOT. All rights reserved.
//

import Foundation

class ARTypeRegistry {
    static let shared = ARTypeRegistry()

    private(set) var typesByName: [String : ARType] = [:]

    func find<T>(type: T.Type) -> ARTypeT<T>? {
        let typename = String(describing: type)
        return typesByName[typename] as? ARTypeT<T>
    }

    @discardableResult func findOrCreate<T>(type: T.Type) -> ARTypeT<T> {
        let typename = String(describing: type)
        if let type = typesByName[typename] as? ARTypeT<T> { return type }
        let newType = ARTypeT(type)
        typesByName[typename] = newType
                
        if !typename.hasPrefix("ARTypeT") {
            let metaClass = findOrCreate(type: ARTypeT<T>.self)
            metaClass.isClass = true
        }
        return newType
    }
    
    func find(typeNamed typename: String) -> ARType? {
        let t = typesByName[typename]
        return t
    }    
}
