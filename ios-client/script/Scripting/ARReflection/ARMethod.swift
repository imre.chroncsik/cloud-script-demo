//
//  ARMethod.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 6/2/20.
//  Copyright © 2020 RYOT. All rights reserved.
//

/*
        
        class methods vs instance methods
        
    Class methods are basically handled as instance methods on the corresponding ARTypeT<MyClass> instance.
    
    (Not as instance methods on the Swift class metaobject (MyClass.self), because those have some quirks,
    most importantly how they behave as objects in methods calls (MyClass.self.staticFoo()),
    but do _not_ conform to AnyObject, so can't be passed around as AnyObject, used as dict keys, etc.
    Worse yet, casting them into AnyObject does succeed
    (actually, you can cast literally anything, including value types, into AnyObject,
    because the automagical Swift <-> ObjC interoperability
    depends on that -- see https://forums.swift.org/t/any-as-anyobject/11182/2)
    but doing so results in surprising behavior:
        print(type(of: X.self))   //  X.Type
        let anyObject: AnyObject = X.self
        print(type(of: anyObject))   //  <moduleName>.X => ".Type" is missing!
    That <moduleName> is the smaller issue, the real deal is the missing ".Type" suffix.
    So we use ARTypeT instead.)
*/

import Foundation


protocol ARMethod {
    static var paramCount: Int { get }

    var signature: [ARType] { get }
    var resultType: ARType { get }
    var paramNames: [String] { get }

    func anyCall(object: AnyObject, params: [Any]) throws -> Any
    
    //  private
    static var defaultParamNames: [String] { get }
    func throwingCast<D>(_ source: Any) throws -> D
    func check(params: [Any]) throws
}

extension ARMethod {
    static var defaultParamNames: [String] { (0 ..< paramCount).map({ "p\($0)" }) }
    
    var resultType: ARType { signature[1] }
    
    func throwingCast<D>(_ source: Any) throws -> D {
        guard let destination = source as? D else {
            throw ARScript_Error.castFailed(
                sourceTypeName: String(describing: type(of: source)),
                destinationTypeName: String(describing: D.self))
        }
        return destination
    }
    
    func check(params: [Any]) throws {
        guard params.count == Self.paramCount
            else { throw ARScript_Error.paramCountMismatch }
    }
}


protocol ARMethod0: ARMethod {
    func anyCall(object: AnyObject) throws -> Any
}

extension ARMethod0 {
    static var paramCount: Int { 0 }
    func anyCall(object: AnyObject, params: [Any]) throws -> Any {
        try check(params: params)
        return try anyCall(object: object)
    }
}

class ARMethod0Implementation<C, R>: ARMethod0 {
    typealias Block = (C) throws -> R

    let signature: [ARType]
    var paramNames: [String] { [] }

    init(block: @escaping Block, signature: [ARType]) {
        self.block = block
        self.signature = signature
    }

    func anyCall(object: AnyObject) throws -> Any {
        let object: C = try throwingCast(object)
        let result = try block(object)
        return result
    }

    private let block: Block
}


protocol ARMethod1: ARMethod {
    func anyCall(object: AnyObject, ap0: Any) throws -> Any
}

extension ARMethod1 {
    static var paramCount: Int { 1 }
    func anyCall(object: AnyObject, params: [Any]) throws -> Any {
        try check(params: params)
        return try anyCall(object: object, ap0: params[0])
    }
}

class ARMethod1Implementation<C, R, P0>: ARMethod1 {
    typealias Block = (C, P0) throws -> R

    let signature: [ARType]
    let paramNames: [String]

    init(block: @escaping Block, signature: [ARType], paramNames: [String]? = nil) {
        self.block = block
        self.signature = signature
        self.paramNames = paramNames ?? Self.defaultParamNames
        assert(self.paramNames.count == Self.paramCount)
    }

    func anyCall(object: AnyObject, ap0: Any) throws -> Any {
        let object: C = try throwingCast(object)
        let p0: P0 = try throwingCast(ap0)
        let result = try block(object, p0)
        return result
    }
    
    private let block: Block
}


protocol ARMethod2: ARMethod {
    func anyCall(object: AnyObject, ap0: Any, ap1: Any) throws -> Any
}

extension ARMethod2 {
    static var paramCount: Int { 2 }
    func anyCall(object: AnyObject, params: [Any]) throws -> Any {
        try check(params: params)
        return try anyCall(object: object, ap0: params[0], ap1: params[1])
    }
}

class ARMethod2Implementation<C, R, P0, P1>: ARMethod2 {
    typealias Block = (C, P0, P1) throws -> R

    let signature: [ARType]
    let paramNames: [String]

    init(block: @escaping Block, signature: [ARType], paramNames: [String]? = nil) {
        self.block = block
        self.signature = signature
        self.paramNames = paramNames ?? Self.defaultParamNames
        assert(self.paramNames.count == Self.paramCount)
    }
    
    func anyCall(object: AnyObject, ap0: Any, ap1: Any) throws -> Any {
        let object: C = try throwingCast(object)
        let p0: P0 = try throwingCast(ap0)
        let p1: P1 = try throwingCast(ap1)
        let result = try block(object, p0, p1)
        return result
    }
   
    private let block: Block
}
