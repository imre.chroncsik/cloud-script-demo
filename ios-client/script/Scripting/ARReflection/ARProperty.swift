//
//  ARProperty.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 6/6/20.
//  Copyright © 2020 RYOT. All rights reserved.
//

import Foundation

/*
        setter vs @escaping
        
    `setter` closure parameters need to be @escaping too, but spelling that out leads to compiler errors.
    fortunately we can simply omit the "@escaping" in this case, because the `setter` parameter
    is already @escaping, as a consequence of being optional.
    
    see https://bugs.swift.org/browse/SR-2444?focusedCommentId=17909&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-17909
    but the main point is that only closures in function-parameter positions are non-@escaping by default,
    all other closures default to @escaping. and if we use a closure type as a generic type parameter,
    then the closure type itself is _not_ in a function-parameter position anymore (it's now in a
    generic-type-parameter position).
    so `foo(block: () -> Void)` => block is non-escaping, but `foo(blocks: Array<() -> Void>)` => escaping.
    `(() -> Void)?` is equivalent to `Optional<() -> Void>`, so `foo(block: (() -> Void)?)` => escaping.
*/

func ar_register<C, T>(
    propertyOfClass: C.Type,
    named name: String,
    setter: ((C, T) -> Void)? = nil,   //  setter vs @escaping => see comments at the top of this file
    getter: @escaping (C) -> T)
    where T: Codable {

    ARTypeRegistry.shared.findOrCreate(type: C.self).isClass = true
    arType(C.self).register(propertyNamed: name)
    let getterMethod = ARMethod0Implementation(block: getter, signature: [arType(C.self), arType(T.self)])
    arType(C.self).register(method: getterMethod, withName: "get__" + name)
    if let setter = setter {
        let setterMethod = ARMethod1Implementation(block: setter, signature: [arType(C.self), arType(Void.self), arType(T.self)])
        arType(C.self).register(method: setterMethod, withName: "set__" + name)
    }
}

