//
//  ARObjectRegistry.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 6/2/20.
//  Copyright © 2020 RYOT. All rights reserved.
//

import Foundation

class ARObjectRegistry {
    enum Error: Swift.Error {
        case idMismatch
    }

    static let shared = ARObjectRegistry()
    
    func object(forId id: String) -> AnyObject? { objectsById[id]?.value }
    
    func id(forObject object: AnyObject) -> String {
        id(forPreviouslyAddedObject: object) ?? newId(forObject: object)
    }

    func id(forPreviouslyAddedObject object: AnyObject) -> String? {
        idsByObject[ObjectIdentifier(object)]
    }

    func register(object: AnyObject, withId objectId: String) throws {
        let previousId = id(forPreviouslyAddedObject: object)
        guard previousId != objectId else { return }
        guard previousId == nil else { throw Error.idMismatch }
        internalRegister(object: object, withId: objectId)
    }
    
    private var objectsById: [String: WeakRef<AnyObject>] = [:]
    private var idsByObject: [ObjectIdentifier: String] = [:]
       
    private func newId(forObject object: AnyObject) -> String {
        let newId = UUID().uuidString
        internalRegister(object: object, withId: newId)
        return newId
    }
    
    private func internalRegister(object: AnyObject, withId objectId: String) {
        assert(id(forPreviouslyAddedObject: object) == nil)
        objectsById[objectId] = WeakRef(value: object)
        idsByObject[ObjectIdentifier(object)] = objectId
    }
}
