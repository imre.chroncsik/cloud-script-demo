//
//  ARType.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 5/28/20.
//  Copyright © 2020 RYOT. All rights reserved.
//

import Foundation


typealias ARTypeId = String
func ar_typeId<T>(of type: T.Type) -> ARTypeId { String(describing: type) }


enum ARTypeError: Error {
    case fromStringNotSet
}

protocol ARTypeFunction {}

protocol ARTypeFunctionF: ARTypeFunction {
    associatedtype Function
    static var name: String { get }
}

protocol ARType: class {
    var name: String { get }
    var swiftMetatype: Any { get }
    var isClass: Bool { get set }
    var methodsByName: [String: ARMethod] { get set }
    var propertyNames: [String] { get set }
    
    var functions: [String: Any] { get set }
    
    func register(method: ARMethod, withName: String)
    func register(propertyNamed: String)
    
    func function<F>(_ f: F.Type) throws -> F.Function where F: ARTypeFunctionF
}

extension ARType {
    func register(method: ARMethod, withName name: String) {
        isClass = true
        methodsByName[name] = method
    }

    func register(propertyNamed name: String) {
        propertyNames.append(name)
    }
}

protocol ARTypeT_: ARType {
    associatedtype T
}

extension ARTypeT {
    var name: String { String(describing: T.self) }
}

class ARTypeT<T>: ARTypeT_ {
    var isClass: Bool = false
    var swiftMetatype: Any { t }
    var methodsByName: [String : ARMethod] = [:]
    var propertyNames: [String] = []
    var functions: [String: Any] = [:]

    //  todo only TypeRegistry.findOrCreate should be able to access this, otherwise should be private
    init(_ t: T.Type) { self.t = t }

    func function<F>(_: F.Type) throws -> F.Function where F: ARTypeFunctionF {
        guard let function = functions[F.name] as? F.Function
            else { throw ARScript_Error.functionNotFound(typeName: self.name, functionName: F.name) }
        return function
    }

    private let t: T.Type
}


func arType<T>(_ t: T.Type, disableCreation: Bool = false) -> ARTypeT<T> {
    return ARTypeRegistry.shared.findOrCreate(type: t)
}
