//
//  ARMethodRegistration.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 6/13/20.
//  Copyright © 2020 RYOT. All rights reserved.
//

import Foundation

/*
    primary registerMethod() variants (by number of parameters)
    *   registering a closure instead of an actual method
        (eg. pass in `block: { $0.foo() }` instead of `method: X.foo`).
        this way we can create new methods too, without having a direct underlying Swift method.
        eg. this is used when registering properties;
        they are technically registered as a pair of methods, a setter and a getter.
        
    all the other registerMethod() variants are basically helper wrappers around this one,
    all calls should ultimately end up in one of these primary variants.
*/

func ar_register<C, R>(
    methodOfClass: C.Type,
    named name: String,
    returnType: ARType? = nil,
    block: @escaping (C) throws -> R) {

    let returnType = returnType ?? arType(R.self)
    let signature = [arType(C.self), returnType]
    let method = ARMethod0Implementation(block: block, signature: signature)
    arType(C.self).register(method: method, withName: name)
}

func ar_register1<C, R, P0>(
    methodOfClass: C.Type,
    named name: String,
    parameterName: String? = nil,
    returnAndParameterTypes: [ARType]? = nil,
    block: @escaping (C, P0) throws -> R) {
    
    let returnAndParameterTypes = returnAndParameterTypes ?? [arType(R.self), arType(P0.self)]
    assert(returnAndParameterTypes.count == 2)
    let signature = [arType(C.self)] + returnAndParameterTypes
    let method = ARMethod1Implementation(block: block, signature: signature, paramNames: parameterName.flatMap{ [$0] })
    arType(C.self).register(method: method, withName: name)
}

func ar_register<C, R, P0, P1>(
    methodOfClass: C.Type,
    named name: String,
    parameterNames: [String]? = nil,
    returnAndParameterTypes: [ARType]? = nil,
    block: @escaping (C, P0, P1) throws -> R) {
    
    let returnAndParameterTypes = returnAndParameterTypes ?? [arType(R.self), arType(P0.self)]
    assert(returnAndParameterTypes.count == 3)
    let signature = [arType(C.self)] + returnAndParameterTypes
    let method = ARMethod2Implementation(block: block, signature: signature, paramNames: parameterNames)
    arType(C.self).register(method: method, withName: name)
}


//  secondary registerMethod() helpers

//  instance method

func ar_register<C, R>(
    method: @escaping (C) -> () throws -> R,
    named name: String,
    returnType: ARType? = nil) {

    ar_register(methodOfClass: C.self, named: name, returnType: returnType, block: { try method($0)() })
}

func ar_register1<C, R, P0>(
    method: @escaping (C) -> (P0) throws -> R,
    named name: String,
    parameterName: String? = nil,
    returnAndParameterTypes: [ARType]? = nil) {

    ar_register1(methodOfClass: C.self, named: name, parameterName: parameterName,
        returnAndParameterTypes: returnAndParameterTypes, block: { try method($0)($1) })
}

func ar_register<C, R, P0, P1>(
    method: @escaping (C) -> (P0, P1) throws -> R,
    named name: String,
    parameterNames: [String]? = nil,
    returnAndParameterTypes: [ARType]? = nil) {

    ar_register(methodOfClass: C.self, named: name, parameterNames: parameterNames,
        returnAndParameterTypes: returnAndParameterTypes, block: { try method($0)($1, $2) })
}


//  static method

func ar_register<C, R>(
    staticMethod: @escaping () -> R,
    ofClass: C.Type,
    named name: String,
    returnType: ARType? = nil) {
    
    arType(C.self).isClass = true
    ar_register(methodOfClass: ARTypeT<C>.self, named: name, returnType: returnType, block: { _ in staticMethod() })
}

func ar_register1<C, R, P0>(
    staticMethod: @escaping (P0) throws -> R,
    ofClass: C.Type,
    named name: String,
    parameterName: String? = nil,
    returnAndParameterTypes: [ARType]? = nil) {

    arType(C.self).isClass = true
    ar_register1(methodOfClass: ARTypeT<C>.self, named: name, parameterName: parameterName,
        returnAndParameterTypes: returnAndParameterTypes, block: { try staticMethod($1) })
}

func ar_register<C, R, P0, P1>(
    staticMethod: @escaping (P0, P1) throws -> R,
    ofClass: C.Type,
    named name: String,
    parameterNames: [String]? = nil,
    returnAndParameterTypes: [ARType]? = nil) {

    arType(C.self).isClass = true
    ar_register(methodOfClass: ARTypeT<C>.self, named: name, parameterNames: parameterNames,
        returnAndParameterTypes: returnAndParameterTypes, block: { try staticMethod($1, $2) })
}


//  init method

/*
    todo rename, move somewhere else.
    while ARObjectRegistry stores weak refs to objects,
    ARScriptCreatedObjectRetainer stores strong refs.
    this is necessary to make sure that objects whose creation was initiated
    by script, have at least one strong ref on the native side.
    otherwise they'd get deallocated on the native side,
    causing object-not-found errors the next time the script tries
    to call native functions on them.
*/
class ARScriptCreatedObjectRetainer {
    static let shared = ARScriptCreatedObjectRetainer()
    func add(object: AnyObject) { objects.append(object) }
    private var objects: [AnyObject] = []
}

func ar_register<C>(initMethod: @escaping () -> C) where C: AnyObject {
    let initAndRetain: () -> C = {
        let newObject = initMethod()
        ARScriptCreatedObjectRetainer.shared.add(object: newObject)
        return newObject
    }
    ar_register(staticMethod: initAndRetain, ofClass: C.self, named: "new")
}

func ar_register1<C, P0>(
    initMethod: @escaping (P0) -> C,
    parameterName: String? = nil,
    parameterType: ARType? = nil)
    where C: AnyObject {
    
    let initAndRetain: (P0) -> C = {
        let newObject = initMethod($0)
        ARScriptCreatedObjectRetainer.shared.add(object: newObject)
        return newObject
    }
    let returnAndParameterTypes: [ARType]? = (parameterType == nil) ? nil : [arType(C.self), parameterType!]
    ar_register1(staticMethod: initAndRetain, ofClass: C.self, named: "new",
        parameterName: parameterName, returnAndParameterTypes: returnAndParameterTypes)
}

func ar_register<C, P0, P1>(
    initMethod: @escaping (P0, P1) -> C,
    parameterNames: [String]? = nil,
    parameterTypes: [ARType]? = nil)
    where C: AnyObject {
    
    let initAndRetain: (P0, P1) -> C = {
        let newObject = initMethod($0, $1)
        ARScriptCreatedObjectRetainer.shared.add(object: newObject)
        return newObject
    }
    let returnAndParameterTypes: [ARType]? = (parameterTypes == nil) ? nil : [arType(C.self)] + parameterTypes!
    ar_register(staticMethod: initAndRetain, ofClass: C.self, named: "new",
        parameterNames: parameterNames, returnAndParameterTypes: returnAndParameterTypes)
}

