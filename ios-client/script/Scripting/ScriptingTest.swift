//
//  ScriptingTest.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 6/22/20.
//

import Foundation
import SceneKit
import ARKit
import QRCodeReader

/*
extension SCNVector3: Codable {
    enum CodingKeys: String, CodingKey { case x; case y; case z }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let x = try container.decode(Float.self, forKey: .x)
        let y = try container.decode(Float.self, forKey: .y)
        let z = try container.decode(Float.self, forKey: .z)
        self.init(x: x, y: y, z: z)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(x, forKey: .x)
        try container.encode(y, forKey: .y)
        try container.encode(z, forKey: .z)
    }
}
*/

//   ---

enum ARSDKScriptError: Error {
    case experienceEntityIsNil
    case objectDefinitionNotFound(name: String)
    case objectInstanceNotFound(uuid: String)
    case animationNotFound(objectDefinition: ObjectDefinition, animationName: String)
}

class ScriptExperience {
    static func export() {
        ar_export(
            method: ScriptExperience.set(interval:callback:),
            named: "setInterval",
            returnAndParameterTypes: [
                ar_export(type: Int.self),
                ar_export(type: TimeInterval.self),
                ar_export(type: (() throws -> Void).self) ])
        ar_export(method: ScriptExperience.getObjectDefinitionNames, named: "getObjectDefinitionNames")
        ar_export1(method: ScriptExperience.getObjectDefinition, named: "getObjectDefinition",
            returnAndParameterTypes: [ar_export(type: ObjectDefinition.self), ar_export(type: String.self)])
        ar_export1(method: ScriptExperience.getObjectInstance, named: "getObjectInstance",
            returnAndParameterTypes: [ar_export(type: ObjectInstance.self), ar_export(type: String.self)])
        ar_export(method: ScriptExperience.getCameraPosition, named: "getCameraPosition")
        ar_export(method: ScriptExperience.getCameraAngles, named: "getCameraAngles")
        ar_export(method: ScriptExperience.getPlanePosition, named: "getPlanePosition")
    }

    weak var scriptingTest: ScriptingTest?
    weak var experienceEntity: ExperienceEntity?
    
    init(scriptingTest: ScriptingTest, experienceEntity: ExperienceEntity) {
        self.scriptingTest = scriptingTest
        self.experienceEntity = experienceEntity
        buildObjectDefinitionsByNameMap()
    }
    
    deinit {
        for timer in timersById.values { timer.invalidate() }
    }
    
    func buildObjectDefinitionsByNameMap() {
        guard let experienceEntity = experienceEntity else { fatalError() }
        for objectEntity in experienceEntity.objects {
            let newObjectDefinition = ObjectDefinition(objectEntity: objectEntity)
            objectDefinitionsByName[objectEntity.uid] = newObjectDefinition
        }
    }
    
    func set(interval: TimeInterval, callback: @escaping (() throws -> Void)) -> Int {
        var timerId: Int = -1
        let startTimer = {
            timerId = self.nextTimerId
            self.nextTimerId += 1
            let timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) {
                _ in
                try? callback()
            }
            self.timersById[timerId] = timer
        }
        isMain() ? startTimer() : DispatchQueue.main.sync(execute: startTimer)
        return timerId
    }

    func getObjectDefinitionNames() throws -> [String] {
        let names = objectDefinitionsByName.keys
        return Array(names).sorted()
    }

    func getObjectDefinition(named name: String) throws -> ObjectDefinition {
        guard let objectDefinition = objectDefinitionsByName[name]
            else { throw ARSDKScriptError.objectDefinitionNotFound(name: name) }
        return objectDefinition
    
//        guard let experienceEntity = experienceEntity else { throw ARSDKScriptError.experienceEntityIsNil }
//        if let objectDefinition = objectDefinitionsByName[name] { return objectDefinition }
//        guard let objectEntity = experienceEntity.objects.first(where: { $0.uid == name })
//            else { throw ARSDKScriptError.objectDefinitionNotFound(name: name) }
//        let newObjectDefinition = ObjectDefinition(objectEntity: objectEntity)
//        objectDefinitionsByName[name] = newObjectDefinition
//        return newObjectDefinition
    }
        
    func createInstance(ofObjectDefinition objectDefinition: ObjectDefinition, withNode node: SCNNode) -> ObjectInstance? {
        guard let uuid = node.uniqueObjectIdentifier else {
            print("node.uuid == nil")
            return nil
        }
        let objectInstance = ObjectInstance(node: node, objectDefinition: objectDefinition)
        objectInstancesByUuid[uuid] = objectInstance
        return objectInstance
    }
    
    func getObjectInstance(forUuid uuid: String) throws -> ObjectInstance {
        guard let objectInstance = objectInstancesByUuid[uuid]
            else { throw ARSDKScriptError.objectInstanceNotFound(uuid: uuid) }
        return objectInstance
    }
    
    func getCameraPosition() -> simd_float3 { scriptingTest?.dependencies?.cameraPosition ?? simd_float3() }
    func getCameraAngles() -> simd_float3 { scriptingTest?.dependencies?.cameraAngles ?? simd_float3() }
    func getPlanePosition() -> simd_float3 { scriptingTest?.dependencies?.planeTransform.translation ?? simd_float3() }

    private var timersById: [Int: Timer] = [:]
    private var nextTimerId = 0
    private var objectDefinitionsByName: [String: ObjectDefinition] = [:]
    private var objectInstancesByUuid: [String: ObjectInstance] = [:]
}

class ObjectDefinition {
    static func export() {
        ar_export1(
            method: ObjectDefinition.setWhenInstanceCreated(callback:),
            named: "whenInstanceCreated",
            returnAndParameterTypes: [
                ar_export(type: Void.self),
                ar_export(type: ((ObjectInstance) throws -> Void).self, returnAndParameterTypes: [
                    ar_export(type: Void.self),
                    ar_export(type: ObjectInstance.self)
                ])
            ])
    }

    let objectEntity: ObjectEntity
    
    init(objectEntity: ObjectEntity) {
        self.objectEntity = objectEntity
    }

    func setWhenInstanceCreated(callback: @escaping (ObjectInstance) throws -> Void) {
        whenInstanceCratedCallback = callback
    }
    
    func instanceWasCreated(_ objectInstance: ObjectInstance) {
        try? whenInstanceCratedCallback?(objectInstance)
    }
    
    private var whenInstanceCratedCallback: ((ObjectInstance) throws -> Void)? = nil
}

class ObjectInstance {
    static func export() {
        ar_export(method: ObjectInstance.getPosition, named: "getPosition")
        ar_export1(method: ObjectInstance.setPosition, named: "setPosition")
        ar_register(propertyOfClass: ObjectInstance.self, named: "position", setter: { $0.position = $1 }, getter: { $0.position })
        
        ar_export1(method: ObjectInstance.setOpacity(_:), named: "setOpacity")
        ar_export1(method: ObjectInstance.setScale(_:), named: "setScale")
        
        ar_export1(
            method: ObjectInstance.setWhenPlaced(callback:),
            named: "whenPlaced",
            returnAndParameterTypes: [
                ar_export(type: Void.self),
                ar_export(type: (() throws -> Void).self) ])
        ar_export1(
            method: ObjectInstance.setWhenDeleted(callback:),
            named: "whenDeleted",
            returnAndParameterTypes: [
                ar_export(type: Void.self),
                ar_export(type: (() throws -> Void).self) ])
        ar_export1(
            method: ObjectInstance.setWhenSelected(callback:),
            named: "whenSelected",
            returnAndParameterTypes: [
                ar_export(type: Void.self),
                ar_export(type: (() throws -> Void).self) ])
        ar_export1(
            method: ObjectInstance.setWhenTapped(callback:),
            named: "whenTapped",
            returnAndParameterTypes: [
                ar_export(type: Void.self),
                ar_export(type: (() throws -> Void).self) ])
                
        ar_export(method: ObjectInstance.getAnimationNames, named: "getAnimationNames")
        ar_export1(method: ObjectInstance.playAnimation(named:), named: "playAnimation")
        ar_export(method: ObjectInstance.stopAllAnimations, named: "stopAllAnimations")
    }

    init(node: SCNNode, objectDefinition: ObjectDefinition) {
        self.node = node
        self.objectDefinition = objectDefinition
    }
    
    func setWhenPlaced(callback: @escaping () throws -> Void) { whenPlacedCallback = callback }
    func setWhenDeleted(callback: @escaping () throws -> Void) { whenDeletedCallback = callback }
    func setWhenSelected(callback: @escaping () throws -> Void) { whenSelectedCallback = callback }
    func setWhenTapped(callback: @escaping () throws -> Void) { whenTappedCallback = callback }
    
    func getAngles() -> simd_float3 { return node.simdEulerAngles }

    func getPosition() -> simd_float3 { return node.simdPosition }
    func setPosition(_ p: simd_float3) { node.simdPosition = p }
    var position: simd_float3 {
        get { node.simdPosition }
        set { node.simdPosition = newValue }
    }
    
    func setOpacity(_ opacity: CGFloat) { node.opacity = opacity }
    func setScale(_ scale: simd_float3) { node.simdScale = scale }

    func getAnimationNames() -> [String] {
        return Array(node.allAnimations.keys).sorted()
    }

    func playAnimation(named animationName: String) throws {
        let scene = try objectDefinition.objectEntity.loadScene()
        guard let animation = scene.rootNode.allAnimations[animationName]
            else { throw ARSDKScriptError.animationNotFound(objectDefinition: objectDefinition, animationName:animationName) }
        node.addAnimation(animation, forKey: animationName)
    }
    
    func stopAllAnimations() throws {
        node.removeAllAnimations()
    }
    
    func didChangeState(from: ARNodeState, to: ARNodeState) {
        if (from == .spawning && to == .selected && !hasBeenPlaced) {
            hasBeenPlaced = true
            try? whenPlacedCallback?()
        }
        if (to == .removed) {
            try? whenDeletedCallback?()
        }
        if (to == .selected) {
            try? whenSelectedCallback?()
        }
    }
    
    func didTap() {
        try? whenTappedCallback?()
    }

    private let node: SCNNode
    private let objectDefinition: ObjectDefinition
    private var hasBeenPlaced = false
    private var whenPlacedCallback: (() throws -> Void)? = nil
    private var whenDeletedCallback: (() throws -> Void)? = nil
    private var whenSelectedCallback: (() throws -> Void)? = nil
    private var whenTappedCallback: (() throws -> Void)? = nil
}


//  ---

protocol ScriptingTestDependencies: class {
//    var cameraTransform: simd_float4x4 { get }
    var cameraPosition: simd_float3 { get }
    var cameraAngles: simd_float3 { get }
    var planeTransform: simd_float4x4 { get }
    
    func present(viewController: UIViewController)
    func dismissPresentedViewController()
    func dismissPresentedViewController(withCompletion: (() -> Void)?)
}

extension ScriptingTestDependencies {
    func dismissPresentedViewController() { dismissPresentedViewController(withCompletion: nil) }
}

class ScriptingTest {
    static let shared = ScriptingTest()
    
    var dependencies: ScriptingTestDependencies? = nil

    init() {
        export()
    }
        
    func export() {
        ARScript_Printer.exportMethods()
        ScriptExperience.export()
        ObjectDefinition.export()
        ObjectInstance.export()
    }
        
    func didStart(experienceEntity: ExperienceEntity) {
        self.experience = ScriptExperience(scriptingTest: self, experienceEntity: experienceEntity)
    
        if ARSDKDebugSettings.shared.remoteScripting {
            scanConnectQR()
        } else {
            scriptExecutor = JSCoreScriptExecutor()
            gScriptExecutor = scriptExecutor
            do {
                try scriptExecutor?.export(object: self.experience!, withGlobalName: "experience")
                try executeScript()
//                try executeScriptFile()
            } catch {
                print(error)
            }
        }
    
        //  --
        /*
        if useRemote {
            scriptExecutor = RemoteScriptExecutor(language: ARScript_JavaScript())
        } else {
            scriptExecutor = JSCoreScriptExecutor()
        }
        gScriptExecutor = scriptExecutor

        self.experience = ScriptExperience(scriptingTest: self, experienceEntity: experienceEntity)
        
        do {
            try scriptExecutor?.export(object: self.experience!, withGlobalName: "ar_experience")
            
//            try executeTestScript()

            if !useRemote {
//                try executeScriptFile()
                try executeScript()
            }
        } catch {
            print(error)
        }
        */
    }
    
    func scanConnectQR() {
        let qrScannerBuilder = QRCodeReaderViewControllerBuilder {
            builder in
            builder.showTorchButton = false
            builder.showSwitchCameraButton = false
            builder.showOverlayView = true
        }
        let qrScannerVC = QRCodeReaderViewController(builder: qrScannerBuilder)

        qrScannerVC.modalPresentationStyle = .formSheet
        qrScannerVC.completionBlock = { [weak self] result in self?.didFinishScanningQR(withResult: result) }
        dependencies?.present(viewController: qrScannerVC)
    }
    
    func didFinishScanningQR(withResult maybeQrResult: QRCodeReaderResult?) {
        guard let qrResult = maybeQrResult else { return }
        let url = qrResult.value
        dependencies?.dismissPresentedViewController {
            [weak self] in
            guard let self = self else { return }
//            self.statusLabel.text = "Connecting..."
            self.connectToCloudScriptingEnvironment(atUrlString: url)
        }
    }
    
    func connectToCloudScriptingEnvironment(atUrlString urlString: String) {
        print("urlString = \(urlString)")
        guard let url = URL(string: urlString) else { fatalError() }
        guard let experience = experience else { fatalError() }
        
        let starscreamClient = RemoteScriptStarscreamClient(url: url)
        self.remoteScriptClient = starscreamClient
        let scriptExecutor = RemoteScriptExecutor(
            dependencies: starscreamClient,
            language: ARScript_JavaScript())
        self.scriptExecutor = scriptExecutor
        gScriptExecutor = scriptExecutor
        
        try? scriptExecutor.export(object: experience, withGlobalName: "experience")
        
        starscreamClient.didConnect = {
            [weak scriptExecutor] in
            DispatchQueue.main.sync {
//                self?.statusLabel.text = "Connected"
            }
            scriptExecutor?.didConnect()
        }
        
        starscreamClient.didReceiveMessage = {
            [weak scriptExecutor] message in
            scriptExecutor?.didReceive(message: message)
        }
        
    }
    
    func restartScript() {
        _ = try? scriptExecutor?.startScript()
    }
    
    func didLeaveExperience() {
        scriptExecutor = nil
        experience = nil
        gScriptExecutor = nil
    }
    
    func executeTestScript() throws {
            let script = """
                arsdk_print("started")
                var sphereDef = ar_experience.getObjectDefinition("sphere")
                arsdk_print("1")
                arsdk_print(sphereDef)
                arsdk_print("2")
                arsdk_print(sphereDef.objectId)
                arsdk_print("3")
                sphereDef.whenInstanceCreated((sphere) => {
                    arsdk_print("sphere created")
                    sphere.scriptOpacity = 0.1
                    sphere.dOpacity = 0.01
                    sphere.whenPlaced(() => {
                        arsdk_print("sphere placed")
                        arsdk_print(JSON.stringify(sphere.position))
                        //sphere.setOpacity(0.5)
                        ar_experience.setInterval(0.01, () => {
                            sphere.scriptOpacity = sphere.scriptOpacity + sphere.dOpacity
                            if (sphere.scriptOpacity > 1) {
                                sphere.scriptOpacity = 1
                                sphere.dOpacity = -0.01
                            }
                            if (sphere.scriptOpacity < 0) {
                                sphere.scriptOpacity = 0
                                sphere.dOpacity = 0.01
                            }
                            sphere.setOpacity(sphere.scriptOpacity)
                        })
                    })
                })
                arsdk_print("running")
                """
                
            try scriptExecutor?.evaluate(script: script)
//            print("x")
    }
    
    func executeScriptFile() throws {
        guard let url = Bundle(for: Self.self).url(forResource: "bundle", withExtension: "js") else { return }
        let script = try String(contentsOf: url)
        try scriptExecutor?.evaluate(script: script)
        scriptExecutor?.didFinishDefiningScriptSideClasses()
        try scriptExecutor?.startScript()
    }
    
    func executeScript() throws {
        guard let scriptSourceAsset = experience?.experienceEntity?.script?.sourceAsset else { return }
        guard let scriptSourceUrl = scriptSourceAsset.cachedURL else { return }
        let scriptSource = try String(contentsOf: scriptSourceUrl)
        try scriptExecutor?.evaluate(script: scriptSource)
        scriptExecutor?.didFinishDefiningScriptSideClasses()
        try scriptExecutor?.startScript()
    }

    func didCreateObject(objectEntity: ObjectEntity, uuid: String, node: SCNNode) {
        guard let experience = experience else { return }
        guard let objectDefinition = try? experience.getObjectDefinition(named: objectEntity.uid) else { return }
        guard let newInstance = experience.createInstance(ofObjectDefinition: objectDefinition, withNode: node) else { return }
        objectDefinition.instanceWasCreated(newInstance)
    }
    
    func objectInstance(forNode node: SCNNode) -> ObjectInstance? {
        guard let uuid = node.uniqueObjectIdentifier else { return nil }
        guard let objectInstance = try? experience?.getObjectInstance(forUuid: uuid) else { return nil }
        return objectInstance
    }
    
    func node(_ node: SCNNode, didChangeStateFrom from: ARNodeState, to: ARNodeState) {
        objectInstance(forNode: node)?.didChangeState(from: from, to: to)
    }
    
    func didTap(node: SCNNode) {
        objectInstance(forNode: node)?.didTap()
    }

//    private var actionColleague: ScriptingTestActionColleague? = nil
    fileprivate var scriptExecutor: ARScript_ScriptExecutor? = nil
//    fileprivate var remoteScriptClient: RemoteScriptClient? = nil
    fileprivate var remoteScriptClient: RemoteScriptStarscreamClient? = nil
    fileprivate var experience: ScriptExperience? = nil
}

class ScriptingTestActionColleague: ExperienceActionColleague {
    init(mediator: ExperienceActionMediator, viewController: ARExperienceViewController) {
        self.viewController = viewController
        super.init(mediator: mediator)
    }

    override func reset() {}

    override func didReceive(message: MediatorMessage, sender: MediatorColleague?) {    
        guard let message = message as? ExperienceActionMessage
            else { return }

        switch message {
            case .experienceStarted(let experienceEntity):
                if let defaultDependencies = ScriptingTest.shared.dependencies as? DefaultScriptingTestDependencies {
                    defaultDependencies.arViewController = viewController
                }
                ScriptingTest.shared.didStart(experienceEntity: experienceEntity)

            case .hudBackButtonTapped:
                ScriptingTest.shared.didLeaveExperience()

            case .objectCreated(let objectEntity, let objectUUID, let objectNode):
                ScriptingTest.shared.didCreateObject(objectEntity: objectEntity, uuid: objectUUID, node: objectNode)

            case .nodeDidChangeState(let node, let fromState, let toState):
                guard let node = node else { return }
                ScriptingTest.shared.node(node, didChangeStateFrom: fromState, to: toState)

            case .didTapNode(let node):
                ScriptingTest.shared.didTap(node: node)

            default: break
        }
    }
    
    private weak var viewController: ARExperienceViewController?
}

/*
class ScriptingTestStateColleague: ExperienceStateColleague {
    override func reset() {}

    override func didReceive(message: MediatorMessage, sender: MediatorColleague?) {
        guard let message = message as? ExperienceStateMessage
            else { return }

        switch message {
            case .didUpdateFrame(let frame):
                break
                

            default: break
        }
    }
}
*/

//  requires someone to set `arViewController` post-construction.
class DefaultScriptingTestDependencies: ScriptingTestDependencies {
    var cameraPosition: simd_float3 { camera?.transform.translation ?? simd_float3() }
    var cameraAngles: simd_float3 { camera?.eulerAngles ?? simd_float3() }
    var camera: ARCamera? { arViewController?.sceneView.engine.currentFrame?.camera }
    
    var planeTransform: simd_float4x4 {
        return arViewController?.managerStore?.planeManager.currentPlaneAnchor?.transform ?? simd_float4x4()
    }

//    weak var sceneViewEngine: SceneViewProtocol?
    weak var arViewController: ARExperienceViewController?

//    init(sceneViewEngine: SceneViewProtocol) {
//        self.sceneViewEngine = sceneViewEngine
//    }
    
    func present(viewController: UIViewController) {
        arViewController?.present(viewController, animated: true)
    }
    
    func dismissPresentedViewController(withCompletion completion: (() -> Void)?) {
        arViewController?.dismiss(animated: true, completion: completion)
    }
}
