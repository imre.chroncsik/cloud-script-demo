//
//  ARSocketConnection.swift
//  script
//
//  Created by Imre Chroncsik on 6/5/20.
//  Copyright © 2020 Ryot. All rights reserved.
//

//  based on https://forums.swift.org/t/socket-api/19971/10

import Foundation
import Network

protocol ARSocketConnectionDelegate: class {
    func connectionDidStop(_ connection: ARSocketConnection, error: Error?)
    func connection(_ connection: ARSocketConnection, didReceiveData data: Data)
    func connectionIsReady(_ connection: ARSocketConnection)
}
extension ARSocketConnectionDelegate {
    func connectionDidStop(_ connection: ARSocketConnection, error: Error?) {}
    func connection(_ connection: ARSocketConnection, didReceiveData data: Data) {}
    func connectionIsReady(_ connection: ARSocketConnection) {}
}

protocol ARSocketConnection: class {
    var delegate: ARSocketConnectionDelegate? { get set }
    var id: Int { get }
    func start()
    func stop(error: Error?)
    func send(data: Data, closeWhenFinished: Bool)
    func send(string: String, closeWhenFinished: Bool)
}

class ARSocketConnectionImplementation: ARSocketConnection {
    weak var delegate: ARSocketConnectionDelegate?
    let id: Int
    private var receiveQueue = DispatchQueue(label: "ARSocketConnection.receiveQueue")

    init(nwConnection: NWConnection) {
        self.nwConnection = nwConnection
        self.id = Self.nextID
        Self.nextID += 1
    }

    private static var nextID: Int = 0

    private let nwConnection: NWConnection

    func start() {
        self.nwConnection.stateUpdateHandler = self.stateDidChange(to:)
        self.setupReceive()
        self.nwConnection.start(queue: receiveQueue)
    }

    func send(data: Data, closeWhenFinished: Bool) {
        self.nwConnection.send(
            content: data,
            isComplete: true,
            completion: .contentProcessed({
                error in
                if let error = error {
                    self.connectionDidFail(error: error)
                    return
                }
                if closeWhenFinished {
                    self.stop(error: nil)
                }
            }))
    }
    
    func send(string: String, closeWhenFinished: Bool) {
        guard let data = string.data(using: .utf8) else { return }
        send(data: data, closeWhenFinished: closeWhenFinished)
    }

    private func stateDidChange(to state: NWConnection.State) {
        switch state {
        case .setup:
            break
        case .waiting(let error):
            self.connectionDidFail(error: error)
        case .preparing:
            break
        case .ready:
            delegate?.connectionIsReady(self)
        case .failed(let error):
            self.connectionDidFail(error: error)
        case .cancelled:
            break
        default:
            break
        }
    }

    private func connectionDidFail(error: Error) {
        print("connection \(self.id) did fail, error: \(error)")
        self.stop(error: error)
    }

    internal func stop(error: Error?) {
        self.nwConnection.stateUpdateHandler = nil
        self.nwConnection.cancel()
        self.delegate?.connectionDidStop(self, error: error)
    }

    private func setupReceive() {
        self.nwConnection.receive(minimumIncompleteLength: 1, maximumLength: 65536) {
            [weak self] (data, _, isComplete, error) in
            guard let self = self else { return }
            if let data = data, !data.isEmpty {
                self.didReceive(data: data)
            }
            if isComplete {
                //  todo close connection if closeWhenFinishedReceiving == true?
            } else if let error = error {
                self.connectionDidFail(error: error)
            } else {
                self.setupReceive()
            }
        }
    }
    
    func didReceive(data: Data) {
        delegate?.connection(self, didReceiveData: data)
    }
}
