//
//  ARSocketServer.swift
//  script
//
//  Created by Imre Chroncsik on 6/5/20.
//  Copyright © 2020 Ryot. All rights reserved.
//

//  based on https://forums.swift.org/t/socket-api/19971/10

import Foundation
import Network


protocol ARSocketServer: class {
    var delegate: ARSocketServerDelegate? { get set }
    var connections: [ARSocketConnection] { get }
    var connection: ARSocketConnection? { get }
    func start()
    func stop()
}

extension ARSocketServer {
    var connection: ARSocketConnection? { connections.last }
}

protocol ARSocketServerDelegate: class {
    func didAccept(connection: ARSocketConnection)
}

class ARSocketServerImplementation: ARSocketServer {
    weak var delegate: ARSocketServerDelegate?
    internal var connections: [ARSocketConnection] = []

    init(port: Int? = nil, publishedServiceType: String? = nil, publishedServiceName: String? = nil) {
        self.publishedServiceType = publishedServiceType
        self.publishedServiceName = publishedServiceName
        
        var nwPort: NWEndpoint.Port = .any
        if let port = port {
            nwPort = NWEndpoint.Port(rawValue: UInt16(port)) ?? nwPort
        }
        listener = try! NWListener(using: .tcp, on: nwPort)
    }

    func start() {
        listener.stateUpdateHandler = { [weak self] in self?.stateDidChange(to: $0) }
        listener.newConnectionHandler = { [weak self] in self?.didAccept(nwConnection: $0) }
        listener.start(queue: .main)
    }

    func stop() {
        listener.stateUpdateHandler = nil
        listener.newConnectionHandler = nil
        listener.cancel()
        for connection in connections {
            connection.stop(error: nil)
        }
        connections.removeAll()
    }

    

    //  private
    
    private let listener: NWListener
    private let publishedServiceType: String?
    private let publishedServiceName: String?
    private var service: NetService? = nil

    private func stateDidChange(to newState: NWListener.State) {
        switch newState {
        case .setup:
            break
        case .waiting:
            break
        case .ready:
            onListenerReady()
            break
        case .failed(let error):
            print("server did fail, error: \(error)")
            self.stop()
        case .cancelled:
            break
        default:
            break
        }
    }

    private func didAccept(nwConnection: NWConnection) {
        let connection = ARSocketConnectionImplementation(nwConnection: nwConnection)
        self.connections.append(connection)
        connection.delegate = self
        connection.start()
        delegate?.didAccept(connection: connection)
    }

    internal func onListenerReady() {
        guard let port = listener.port?.rawValue else { return }
        let service = NetService(domain: "", type: "_arscript._tcp.", name: "arscript", port: Int32(port))
        self.service = service
        service.publish()
    }
}

extension ARSocketServerImplementation: ARSocketConnectionDelegate {
    func connectionDidStop(_ connection: ARSocketConnection, error: Error?) {
        connections.removeAll(where: { $0.id == connection.id })
    }

    func connection(_ connection: ARSocketConnection, didReceiveData data: Data) {}
}

