//
//  ARScript_JavaScript.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 6/2/20.
//  Copyright © 2020 RYOT. All rights reserved.
//

import Foundation

//  todo move this to somewhere else
extension String {
    var quoted: String { "\"\(self)\"" }
}

class ARScript_JavaScript {}

extension ARScript_JavaScript: ARScript_Language {
    func interface() throws -> String {
        var interface = ""
        interface += classes()
        interface += freeFunctions()
        return interface
    }
    
    func scriptForExporting(object: AnyObject, withGlobalName globalName: String?) throws -> String {
        let objectId = ARObjectRegistry.shared.id(forObject: object)
        let className = String(describing: type(of: object))
        guard let _ = ARTypeRegistry.shared.find(typeNamed: className)
            else { throw ARScript_Error.typeNotFound(name: className) }
        let optionalNameParam = (globalName == nil) ? "" : ", \(globalName!.quoted)"
        let script = """
            __arscript_replicateNativeToScript(
                { className: \(className.quoted), objectId: \(objectId.quoted) }
                \(optionalNameParam))
            """
        return script
    }

    func scriptForExecutingCallback(withCallbackJsonString callbackJsonString: String) throws -> String {
        let quotedCallbackJsonData = try JSONEncoder().encode(callbackJsonString)
        guard let quotedCallbackJsonString = String(data: quotedCallbackJsonData, encoding: .utf8)
            else { throw ARScript_Error.jsonDataToStringFailed }
        return "__arscript_executeCallback(\(quotedCallbackJsonString))"
    }

}

private extension ARScript_JavaScript {
/*
    func arscript() -> String {
    
        //  now in a separate .ts file, in a separate repo.
        //  right now that repo is the experience script skeleton repo,
        //  ultimately should be a separate arsdk-js package repo,
        //  and then both the script skeleton, and any integrating app repos
        //  could pull that in as a submodule or whatever.
        fatalError()
    
        /*
            Provides services (as methods on a class called ARScript):
            *	For the implementation of the actual API (eg. executeNative()).
            *	For the execution environment to interact with the script (eg. executeCallback()).
            *	Under the hood it handles object replication and some script / native type conversions.
            *   (Note when doing remote scripting, the same code lives in the `arscript.js` module.)

            Implements these services on top of a relatively narrow set of dependencies:
            *	arsdk__executeNative_str__(callString)
                Execute `callString` on the native side,
                    return the result as a JSON string, with a `value` or an `error` field.

            Usage:
            After creating an instance, you'll also need to set up dependencies.
                var arscript = new ARScript()
                arscript.executeNative_str = my_executeNative_str
                arscript.executeNative(objId, methodName, p0, p1)
        */
        
        return """
            //  dependencies:
            //  *   arsdk__executeNative_str__(callString)
            //      execute `callString` on the native side,
            //      return the result as a JSON string, with a `value` or an `error` field.

            class ARScript {
                executeNative_str = null

                constructor(executeNative_str) {
                    this.executeNative_str = executeNative_str
                }

                executeNative(objectId, methodName, ...params) {
                    while (params.length > 0 && params[params.length - 1] === undefined)
                        params.pop()
                    params = params.map(p => this.scriptValueToNative(p))
                    var strCall = JSON.stringify({
                        "objectId": objectId,
                        "methodName": methodName,
                        "parameters": params.map(p => JSON.stringify(p))
                    })
                    var strResult = this.executeNative_str(strCall)
                    if (strResult === undefined)
                        return undefined
                    var result = JSON.parse(strResult)
                    const isConstructor = methodName === "new"
                    if (result.error !== undefined)
                        throw result.error
                    const resultValue = this.nativeValueToScript(result.value, isConstructor)
                    return resultValue
                }

                executeCallback(callbackJsonString) {
                    try {
                        const callbackObject = JSON.parse(callbackJsonString)
                        const callback = this.callbacks[callbackObject.callbackId]
                        if (callback === undefined)
                            throw "callback not found by id: " + callbackObject.callbackId
                        const params = callbackObject.params.map(p => this.nativeValueToScript(p))
                        const value = callback(...params)
                        return JSON.stringify({ "value": value })
                    } catch(x) {
                        return JSON.stringify({ "error": x.toString() })
                    }
                }

                scriptValueToNative(scriptValue) {
                    if (scriptValue === null)
                        return "arsdk__nil__"

                    if (typeof scriptValue === "function") {
                        const callbackId = this.nextCallbackId++
                        this.callbacks[callbackId] = scriptValue
                        return callbackId
                    }
                    
                    return scriptValue
                }

                nativeValueToScript(nativeValue, isConstructorResult) {
                    if (nativeValue === "()")
                        return undefined
                        
                    if (typeof nativeValue === "string"
                        && nativeValue.indexOf("arsdk__objectId_") !== -1) {
                        const objectId = nativeValue.substring("arsdk__objectId_".length)
                        return objectId
                    }
                        
                    let parsed = JSON.parse(nativeValue)
                    if (typeof parsed === "object"
                        && parsed.arsdk_objectId !== undefined
                        && parsed.arsdk_className !== undefined) {
                        
                        const nativeObject = { className: parsed.arsdk_className, objectId: parsed.arsdk_objectId }
                        if (isConstructorResult) {
                            return nativeObject
                        } else {
                            var object = this.objectsById[nativeObject.objectId]
                            if (object === undefined)
                                object = this.replicateNativeToScript(nativeObject)
                            return object
                        }
                    }
                    
                    return parsed
                }

                replicateScriptToNativeChecked(newObject, className, ...params) {
                    if (params.length > 0 && params[0] === this.arsdk__replicatingNativeToScript_) return
                    this.replicateScriptToNativeUnchecked(newObject, className, ...params)
                }

                replicateScriptToNativeUnchecked(newObject, className, ...params) {
                    const nativeObject = this.executeNative("arsdk_class_" + className, "new", false, ...params)
                    this.setObjectId(newObject, nativeObject.arsdk_objectId)
                }

                replicateNativeToScript(nativeObject) {
                    const class_ = this.classesByName[nativeObject.className]
                    const newObject = new class_(this.arsdk__replicatingNativeToScript_)
                    this.setObjectId(newObject, nativeObject.objectId)
                    return newObject
                }

                setObjectId(object, objectId) {
                    object.objectId = objectId
                    this.objectsById[objectId] = object
                }

                arsdk__replicatingNativeToScript_ = "arsdk__replicatingNativeToScript_"
                userMainFunction = null
                classesByName = {}
                objectsById = {}
                nextCallbackId = 0
                callbacks = {}
            }
            """
    }
*/
        
    func classes() -> String {
        var classes = ""
        for (typeName, arType) in ARTypeRegistry.shared.typesByName {
            if !arType.isClass { continue }
            guard let metaClass = ARTypeRegistry.shared.find(typeNamed: "ARTypeT<\(arType.name)>") else { continue }

            classes += "class \(typeName) {\n"
            classes += staticFunctions(forClass: arType, metaClass: metaClass)
            classes += properties(forClass: arType)
            classes += constructor(forClass: arType, metaClass: metaClass)
            classes += functions(forClass: arType)
            classes += "}\n"
            classes += "arsdk_classesByName[\(typeName.quoted)] = \(typeName)"
            classes += "\n"
        }
        return classes
    }
            
    func staticFunctions(forClass class_: ARType, metaClass: ARType) -> String {
        var staticFunctions = ""
        for (methodName, method) in metaClass.methodsByName {
            if methodName == "new" { continue }
            let paramList = method.paramNames.joined(separator: ", ")
            let executeArgList = (
                ["arsdk_class_\(class_.name)".quoted, methodName.quoted]
                + method.paramNames)
                .joined(separator: ", ")
            staticFunctions += "\tstatic \(methodName)(\(paramList)) { return arsdk_executeNative(\(executeArgList)) }\n"
        }
        return staticFunctions
    }
    
    func properties(forClass class_: ARType) -> String {
        var properties = ""
        for propertyName in class_.propertyNames {
            properties += "\tget \(propertyName)() { return arsdk_executeNative(this.objectId, \"get__\(propertyName)\") }\n"
            let hasSetter = class_.methodsByName["set__" + propertyName] != nil
            if hasSetter {
                properties += "\tset \(propertyName)(newValue) { arsdk_executeNative(this.objectId, \"set__\(propertyName)\", newValue) }\n"
            }
        }
        return properties
    }
    
    func constructor(forClass class_: ARType, metaClass: ARType) -> String {
        var ctorParamNames = metaClass.methodsByName["new"]?.paramNames ?? []
        if ctorParamNames.isEmpty { ctorParamNames = ["objectId"] }
        let paramList = ctorParamNames.joined(separator: ", ")
        let registerArgList = (["this", class_.name.quoted] + ctorParamNames).joined(separator: ", ")
        let ctor = "\tconstructor(\(paramList)) { arsdk_didConstruct(\(registerArgList)) }\n"
        return ctor
    }
    
    func functions(forClass class_: ARType) -> String {
        var functions = ""
        for (methodName, method) in class_.methodsByName {
            if methodName.hasPrefix("get__") || methodName.hasPrefix("set__") { continue }
            let paramList = method.paramNames.joined(separator: ", ")
            let executeArgList = (["this.objectId", methodName.quoted] + method.paramNames).joined(separator: ", ")
            functions += "\t\(methodName)(\(paramList)) { return arsdk_executeNative(\(executeArgList)) }\n"
        }
        return functions
    }

    func freeFunctions() -> String {
        var functions = ""
        functions += """
            function print(str) {
                console.log(str.toString())
                ARScript_Printer.print(str.toString())
            }
            """
        return functions
    }
}
