//
//  Language.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 6/5/20.
//  Copyright © 2020 RYOT. All rights reserved.
//

import Foundation

enum ARScript_LanguageError: Error {
    case unexpected
    case missingDependencies
}

protocol ARScript_Language: class {
    func interface() throws -> String
    func scriptForExporting(object: AnyObject, withGlobalName: String?) throws -> String
    func scriptForExecutingCallback(withCallbackJsonString: String) throws -> String
}

