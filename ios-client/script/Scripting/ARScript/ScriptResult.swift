//
//  ScriptResult.swift
//  script
//
//  Created by Imre Chroncsik on 6/5/20.
//  Copyright © 2020 Ryot. All rights reserved.
//

import Foundation

struct ARScript_StringResult: Codable {
    let value: String?
    let error: String?

    init(value: String?, error: String?) {
        self.value = value
        self.error = error
    }
        
    init(value: String) { self.init(value: value, error: nil) }
    init(error: String) { self.init(value: nil, error: error) }
    init(error: Error) { self.init(value: nil, error: String(describing: error)) }

    init(jsonString: String) {
        do {
            guard let jsonData = jsonString.data(using: .utf8) else { throw ARScript_Error.unexpected }
            let tmp = try JSONDecoder().decode(ARScript_StringResult.self, from: jsonData)
            self.value = tmp.value
            self.error = tmp.error
        } catch {
            self.value = nil
            self.error = String(describing: error)
        }
    }

    var jsonString: String {
        var jsonString = "{}"
        if let jsonData = try? JSONEncoder().encode(self) {
            jsonString = String(data: jsonData, encoding: .utf8) ?? jsonString
        }
        return jsonString
    }

}
