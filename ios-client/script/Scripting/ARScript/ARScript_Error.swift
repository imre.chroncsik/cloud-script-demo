//
//  ARScript_Error.swift
//  script
//
//  Created by Imre Chroncsik on 6/2/20.
//  Copyright © 2020 Ryot. All rights reserved.
//

import Foundation

enum ARScript_Error: Error {
    case unexpected
    case notSupported
    case scriptException(description: String)
    case typeNotFound(name: String)
    case methodNotFound(name: String)
    case objectNotFound(id: String)
    
    //  object is Any, not AnyObject, to support class methods, see comments at the top of ARMethod.swift
    case objectClassMismatch(object: Any, expectedClassName: String)
    
    case paramCountMismatch
    case paramConversionFailed(fromString: String)
    case conversionFailed(fromString: String, toType: String)
    case functionNotFound(typeName: String, functionName: String)
    case castFailed(sourceTypeName: String, destinationTypeName: String)
    case typeMismatch
    case jsonStringToDataFailed
    case jsonDataToStringFailed
    
    var localizedDescription: String {
        switch self {
            case .unexpected: return "unexpected"
            case .notSupported: return "not supported"
            case .scriptException(let description): return "script has thrown exception: \(description)"
            case .typeNotFound(let typeName): return "type not found: \(typeName)"
            case .methodNotFound(let methodName): return "method not found: \(methodName)"
            case .objectNotFound(let objectId): return "object not found with id: \(objectId)"
            case .objectClassMismatch(let object, let expectedClassName):
                return "object class mismatch, expected: \(expectedClassName), actual: \(String(describing: type(of: object)))"
            case .paramCountMismatch: return "param count mismatch"
            case .paramConversionFailed(let fromString): return "param conversion failed from string: \(fromString)"
            case .conversionFailed(let fromString, let toType): return "conversion to type `\(toType)` failed from string: \(fromString)"
            case .functionNotFound(let typeName, let functionName): return "dynamic function not found: \(typeName).\(functionName)"
            case .castFailed(let sourceTypeName, let destinationTypeName):
                return "cast failed: \(sourceTypeName) -> \(destinationTypeName)"
            case .typeMismatch: return "type mismatch"
            case .jsonStringToDataFailed: return "jsonStringToDataFailed"
            case .jsonDataToStringFailed: return "jsonDataToStringFailed"
        }
    }
}

