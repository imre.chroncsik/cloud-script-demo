//
//  ARScriptExportMethod.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 6/16/20.
//  Copyright © 2020 RYOT. All rights reserved.
//

import Foundation

/*
    primary -- explicit signature, block.
    
    note that unlike with register(method:...), the explicit signature
    is mandatory here. the reason is that the inferred-signature version
    will also have some protocol constraints, so that it can automatically
    register the required string conversion functions for the types involved.
    on the other hand, we want the primary export()s to be usable for
    all types, including those not conforming to Codable,
    so no constraints here.
    
    as a rule of thumb, use the inferred-siganture versions for methods
    where all param and return types are simple types that support string
    conversions by protocol conformance, and only directly call the primary
    export()s if any of the parameter types don't conform to Codable
    (but has an appropriate addFromScriptString() overload), for example
    if any of the parameters of the exported function is a function type.
    in such cases you'll also need to wrap these types into
    addFromScriptString() calls:
        TODO example
    (export() can't call addFromString() because of type erasure.)
*/

func ar_export<C, R>(
    methodOfClass: C.Type,
    named name: String,
    returnType: ARExportedType,
    block: @escaping (C) throws -> R)
    where C: AnyObject {
    
    ar_export(type: C.self)
    ar_register(methodOfClass: C.self, named: name,
        returnType: returnType.arType, block: block)
}

/*
    for all other arities we use the function name `ar_export()`,
    but for on parameter, we use `ar_export1()`.
    the reason is that otherwise the single P0 type parameter
    could be inferred by the compiler as a tuple,
    thus matching any number of parameters,
    so the compiler would call the one-param `ar_export()`
    (with that one param being a tuple)
    instead of the correct n-params overload.
    
    todo could it be fixed by adding an extra pair of parentheses
    around P0? would that decompose the tuple, so it now only matches
    a tuple with exactly one element?
*/
func ar_export1<C, R, P0>(
    methodOfClass: C.Type,
    named name: String,
    parameterName: String? = nil,
    returnAndParameterTypes: [ARExportedType],
    block: @escaping (C, P0) throws -> R)
    where C: AnyObject {
        
    ar_export(type: C.self)
    ar_register1(methodOfClass: C.self, named: name, parameterName: parameterName,
        returnAndParameterTypes: returnAndParameterTypes.map{ $0.arType }, block: block)
}

func ar_export<C, R, P0, P1>(
    methodOfClass: C.Type,
    named name: String,
    parameterNames: [String]? = nil,
    returnAndParameterTypes: [ARExportedType],
    block: @escaping (C, P0, P1) throws -> R)
    where C: AnyObject {
    
    ar_export(type: C.self)
    ar_register(methodOfClass: C.self, named: name, parameterNames: parameterNames,
        returnAndParameterTypes: returnAndParameterTypes.map{ $0.arType }, block: block)
}


//  instance method, explicit signature

func ar_export<C, R>(
    method: @escaping (C) -> () throws -> R,
    named name: String,
    returnType: ARExportedType)
    where C: AnyObject {

    ar_export(type: C.self)
    ar_register(method: method, named: name, returnType: returnType.arType)
}

func ar_export1<C, R, P0>(
    method: @escaping (C) -> (P0) throws -> R,
    named name: String,
    parameterName: String? = nil,
    returnAndParameterTypes: [ARExportedType])
    where C: AnyObject {

    ar_export(type: C.self)
    ar_register1(method: method, named: name, parameterName: parameterName,
        returnAndParameterTypes: returnAndParameterTypes.map{ $0.arType })
}

func ar_export<C, R, P0, P1>(
    method: @escaping (C) -> (P0, P1) throws -> R,
    named name: String,
    parameterNames: [String]? = nil,
    returnAndParameterTypes: [ARExportedType])
    where C: AnyObject {

    ar_export(type: C.self)
    ar_register(method: method, named: name, parameterNames: parameterNames,
        returnAndParameterTypes: returnAndParameterTypes.map{ $0.arType })
}

/*
    instance method, inferred signature.
    these require all param types to conform to Codable
    (if a param type does not conform to InitFromStr, then the version of export()
    taking an explicitly defined signature should be used.)
*/

func ar_export<C, R>(
    method: @escaping (C) -> () throws -> R,
    named name: String)
    where C: AnyObject, R: Codable {

    ar_export(method: method, named: name, returnType: ar_export(type: R.self))
}

func ar_export1<C, R, P0>(
    method: @escaping (C) -> (P0) throws -> R,
    named name: String,
    parameterName: String? = nil)
    where C: AnyObject, R: Codable, P0: Codable {

    let returnAndParameterTypes: [ARExportedType] = [ar_export(type: R.self), ar_export(type: P0.self)]
    ar_export1(method: method, named: name,
        parameterName: parameterName, returnAndParameterTypes: returnAndParameterTypes)
}

func ar_export<C, R, P0, P1>(
    method: @escaping (C) -> (P0, P1) throws -> R,
    named name: String,
    parameterNames: [String]? = nil)
    where C: AnyObject, R: Codable, P0: Codable, P1: Codable {

    let returnAndParameterTypes: [ARExportedType] = [ar_export(type: R.self), ar_export(type: P0.self), ar_export(type: P1.self)]
    ar_export(method: method, named: name,
        parameterNames: parameterNames, returnAndParameterTypes: returnAndParameterTypes)
}


//  instance method, inferred signature, R == Void

func ar_export<C>(
    method: @escaping (C) -> () throws -> Void,
    named name: String)
    where C: AnyObject {

    ar_export(method: method, named: name, returnType: ar_export(type: Void.self))
}

func ar_export1<C, P0>(
    method: @escaping (C) -> (P0) throws -> Void,
    named name: String,
    parameterName: String? = nil)
    where C: AnyObject, P0: Codable {

    let returnAndParameterTypes: [ARExportedType] = [ar_export(type: Void.self), ar_export(type: P0.self)]
    ar_export1(method: method, named: name,
        parameterName: parameterName, returnAndParameterTypes: returnAndParameterTypes)
}

func ar_export<C, P0, P1>(
    method: @escaping (C) -> (P0, P1) throws -> Void,
    named name: String,
    parameterNames: [String]? = nil)
    where C: AnyObject, P0: Codable, P1: Codable {

    let returnAndParameterTypes: [ARExportedType] = [ar_export(type: Void.self), ar_export(type: P0.self), ar_export(type: P1.self)]
    ar_export(method: method, named: name,
        parameterNames: parameterNames, returnAndParameterTypes: returnAndParameterTypes)
}

//  static method, explicit signature

func ar_export<C, R>(
    staticMethod: @escaping () -> R,
    ofClass: C.Type,
    named name: String,
    returnType: ARExportedType)
    where C: AnyObject {

    ar_export(type: C.self)
    ar_register(staticMethod: staticMethod, ofClass: ofClass, named: name,
        returnType: returnType.arType)
}

func ar_export1<C, R, P0>(
    staticMethod: @escaping (P0) throws -> R,
    ofClass: C.Type,
    named name: String,
    parameterName: String? = nil,
    returnAndParameterTypes: [ARExportedType])
    where C: AnyObject {

    ar_export(type: C.self)
    ar_register1(staticMethod: staticMethod, ofClass: ofClass, named: name, parameterName: parameterName,
        returnAndParameterTypes: returnAndParameterTypes.map{ $0.arType })
}

func ar_export<C, R, P0, P1>(
    staticMethod: @escaping (P0, P1) throws -> R,
    ofClass: C.Type,
    named name: String,
    parameterNames: [String]? = nil,
    returnAndParameterTypes: [ARExportedType])
    where C: AnyObject {

    ar_export(type: C.self)
    ar_register(staticMethod: staticMethod, ofClass: ofClass, named: name, parameterNames: parameterNames,
        returnAndParameterTypes: returnAndParameterTypes.map{ $0.arType })
}


//  static method, inferred signature

func ar_export<C, R>(
    staticMethod: @escaping () -> R,
    ofClass: C.Type,
    named name: String)
    where C: AnyObject, R: Codable {

    ar_export(staticMethod: staticMethod, ofClass: ofClass, named: name, returnType: ar_export(type: R.self))
}

func ar_export1<C, R, P0>(
    staticMethod: @escaping (P0) throws -> R,
    ofClass: C.Type,
    named name: String,
    parameterName: String? = nil)
    where C: AnyObject, R: Codable, P0: Codable {

    let returnAndParameterTypes: [ARExportedType] = [ar_export(type: R.self), ar_export(type: P0.self)]
    ar_export1(staticMethod: staticMethod, ofClass: ofClass, named: name,
        parameterName: parameterName, returnAndParameterTypes: returnAndParameterTypes)
}

func ar_export<C, R, P0, P1>(
    staticMethod: @escaping (P0, P1) throws -> R,
    ofClass: C.Type,
    named name: String,
    parameterNames: [String]? = nil)
    where C: AnyObject, R: Codable, P0: Codable, P1: Codable {

    let returnAndParameterTypes: [ARExportedType] = [ar_export(type: R.self), ar_export(type: P0.self), ar_export(type: P1.self)]
    ar_export(staticMethod: staticMethod, ofClass: ofClass, named: name,
        parameterNames: parameterNames, returnAndParameterTypes: returnAndParameterTypes)
}


//  static method, inferred signature, R == Void

func ar_export<C>(
    staticMethod: @escaping () -> Void,
    ofClass: C.Type,
    named name: String)
    where C: AnyObject {

    ar_export(staticMethod: staticMethod, ofClass: ofClass, named: name, returnType: ar_export(type: Void.self))
}

func ar_export1<C, P0>(
    staticMethod: @escaping (P0) throws -> Void,
    ofClass: C.Type,
    named name: String,
    parameterName: String? = nil)
    where C: AnyObject, P0: Codable {

    let returnAndParameterTypes: [ARExportedType] = [ar_export(type: Void.self), ar_export(type: P0.self)]
    ar_export1(staticMethod: staticMethod, ofClass: ofClass, named: name,
        parameterName: parameterName, returnAndParameterTypes: returnAndParameterTypes)
}

func ar_export<C, P0, P1>(
    staticMethod: @escaping (P0, P1) throws -> Void,
    ofClass: C.Type,
    named name: String,
    parameterNames: [String]? = nil)
    where C: AnyObject, P0: Codable, P1: Codable {

    let returnAndParameterTypes: [ARExportedType] = [ar_export(type: Void.self), ar_export(type: P0.self), ar_export(type: P1.self)]
    ar_export(staticMethod: staticMethod, ofClass: ofClass, named: name,
        parameterNames: parameterNames, returnAndParameterTypes: returnAndParameterTypes)
}


//  init, explicit signature

func ar_export<C>(
    initMethod: @escaping () -> C)
    where C: AnyObject {
    
    ar_export(type: C.self)
    ar_register(initMethod: initMethod)
}

func ar_export1<C, P0>(
    initMethod: @escaping (P0) -> C,
    parameterName: String? = nil,
    parameterType: ARExportedType)
    where C: AnyObject {
    
    ar_export(type: C.self)
    ar_register1(initMethod: initMethod, parameterName: parameterName,
        parameterType: parameterType.arType)
}

func ar_export<C, P0, P1>(
    initMethod: @escaping (P0, P1) -> C,
    parameterNames: [String]? = nil,
    parameterTypes: [ARExportedType])
    where C: AnyObject {

    ar_export(type: C.self)
    ar_register(initMethod: initMethod, parameterNames: parameterNames,
        parameterTypes: parameterTypes.map{ $0.arType })
}


//  init, inferred signature

func ar_export1<C, P0>(
    initMethod: @escaping (P0) -> C,
    parameterName: String? = nil)
    where C: AnyObject, P0: Codable {

    ar_export1(initMethod: initMethod, parameterName: parameterName, parameterType: ar_export(type: P0.self))
}

func ar_export<C, P0, P1>(
    initMethod: @escaping (P0, P1) -> C,
    parameterNames: [String]? = nil)
    where C: AnyObject, P0: Codable, P1: Codable {

    let parameterTypes: [ARExportedType] = [ar_export(type: P0.self), ar_export(type: P1.self)]
    ar_export(initMethod: initMethod, parameterNames: parameterNames, parameterTypes: parameterTypes)
}

