//
//  ARScriptExport.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 6/11/20.
//  Copyright © 2020 RYOT. All rights reserved.
//

import Foundation

/*
    the bodies of all these specializations are exactly the same,
    they need to be specialized to make sure that the correct version of
    addFromScriptString() gets called. without also specializing export(),
    T inside export() would be an unknown type, so addFromScriptString()
    would call the primary, unspecialized version of addFromStringString.
*/

@discardableResult func ar_export<T>(type: T.Type) -> ARExportedType
    where T: Codable {
    return ARExportedType(arType: addFromScriptString(toARType: arType(type)))
}

@discardableResult func ar_export<T>(type: T.Type) -> ARExportedType
    where T: AnyObject {
    return ARExportedType(arType: addFromScriptString(toARType: arType(type)))
}

@discardableResult func ar_export(type: Void.Type) -> ARExportedType {
    return ARExportedType(arType: addFromScriptString(toARType: arType(type)))
}

@discardableResult func ar_export<T>(type: T?.Type, wrappedType: ARExportedType) -> ARExportedType {
    assert(wrappedType.arType === arType(T.self))
    return ARExportedType(arType: addFromScriptString(toARType: arType(type)))
}

@discardableResult func ar_exportOptional<T>(type: T.Type) -> ARExportedType
    where T: Codable {
    return ar_export(type: T?.self, wrappedType: ar_export(type: T.self))
}

@discardableResult func ar_exportOptional<T>(type: T.Type) -> ARExportedType
    where T: AnyObject {
    return ar_export(type: T?.self, wrappedType: ar_export(type: T.self))
}


//  function types with explicit signature

@discardableResult func ar_export<R>(type: (() throws -> R).Type, returnType: ARExportedType) -> ARExportedType {
    return ARExportedType(arType: addFromScriptString(toARType: arType(type)))
}

@discardableResult func ar_export<R, P0>(type: ((P0) throws -> R).Type,
    returnAndParameterTypes: [ARExportedType]) -> ARExportedType {
    return ARExportedType(arType: addFromScriptString(toARType: arType(type)))
}

@discardableResult func ar_export<R, P0, P1>(type: ((P0, P1) throws -> R).Type,
    returnAndParameterTypes: [ARExportedType]) -> ARExportedType {
    return ARExportedType(arType: addFromScriptString(toARType: arType(type)))
}


//  function types with inferred signature (all return and param types conform to InitFromStr)

@discardableResult func ar_export<R>(type: (() throws -> R).Type) -> ARExportedType
    where R: Codable {
    return ar_export(type: type, returnType: ar_export(type: R.self))
}

@discardableResult func ar_export<R, P0>(type: ((P0) throws -> R).Type) -> ARExportedType
    where R: Codable, P0: Codable {
    return ar_export(type: type, returnAndParameterTypes: [ar_export(type: R.self), ar_export(type: P0.self)])
}

@discardableResult func ar_export<R, P0, P1>(type: ((P0, P1) throws -> R).Type) -> ARExportedType
    where R: Codable, P0: Codable, P1: Codable {
    let returnAndParameterTypes = [ar_export(type: R.self), ar_export(type: P0.self), ar_export(type: P1.self)]
    return ar_export(type: type, returnAndParameterTypes: returnAndParameterTypes)
}


//  function types with inferred signature, R == Void (all param types conform to InitFromStr)

@discardableResult func ar_export(type: (() throws -> Void).Type) -> ARExportedType {
    return ar_export(type: type, returnType: ar_export(type: Void.self))
}

@discardableResult func ar_export<P0>(type: ((P0) throws -> Void).Type) -> ARExportedType
    where P0: Codable {
    return ar_export(type: type, returnAndParameterTypes: [ar_export(type: Void.self), ar_export(type: P0.self)])
}

@discardableResult func ar_export<P0, P1>(type: ((P0, P1) throws -> Void).Type) -> ARExportedType
    where P0: Codable, P1: Codable {
    let returnAndParameterTypes = [ar_export(type: Void.self), ar_export(type: P0.self), ar_export(type: P1.self)]
    return ar_export(type: type, returnAndParameterTypes: returnAndParameterTypes)
}



struct ARExportedType {
    let arType: ARType
}

