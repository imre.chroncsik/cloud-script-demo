//
//  ARScriptStringConversion.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 6/15/20.
//  Copyright © 2020 RYOT. All rights reserved.
//

import Foundation

/*
    todo document why we're using free functions,
    instead of conforming to a FromScriptString protocol.
    (can't use generic overloads for methods,
    can't create protocol-consrtained exts for non-nominal types.)
    
    also document the role of Codable here.
    (default behavior for types where it can be used,
    while the fromString() overloads serve as a fallback
    for types that can't conform to InitFromString.)
*/


//  FromScriptString type functions

class ARScript_AnyFromScriptString: ARTypeFunctionF {
    typealias Function = (String) throws -> Any
    static var name: String { "anyFromScriptString" }
}

extension ARType {
    func any(fromScriptString string: String) throws -> Any {
        try function(ARScript_AnyFromScriptString.self)(string)
    }
}

class FromScriptString<T>: ARTypeFunctionF {
    typealias Function = (String) throws -> T
    static var name: String { "fromScriptString" }
}

extension ARTypeT {
    func from(scriptString: String) throws -> T {
        try function(FromScriptString<T>.self)(scriptString)
    }
}


class ScriptStringFromAny: ARTypeFunctionF {
    typealias Function = (Any) throws -> String
    static var name: String { "scriptStringFromAny" }
}

extension ARType {
    func scriptString(fromAny any: Any) throws -> String {
        try function(ScriptStringFromAny.self)(any)
    }
}

class ScriptStringFrom<T>: ARTypeFunctionF {
    typealias Function = (T) throws -> String
    static var name: String { "scriptStringFrom" }
}

extension ARTypeT {
    func scriptString(from t: T) throws -> String {
        try function(ScriptStringFrom<T>.self)(t)
    }
}



//  todo remove, use ARScript_Error instead?
enum FromScriptStringError: Error {
    case failed
    case converterNotFound
}

//  --- addFromScriptString() ---

@discardableResult func use<T>(
    fromScriptString: @escaping FromScriptString<T>.Function,
    scriptStringFrom: @escaping ScriptStringFrom<T>.Function,
    forARType arType: ARTypeT<T>)
    -> ARTypeT<T> {
        
    arType.functions[FromScriptString<T>.name] = fromScriptString
    arType.functions[ScriptStringFrom<T>.name] = scriptStringFrom

    let anyFromScriptString: ARScript_AnyFromScriptString.Function = { try fromScriptString($0) }
    arType.functions[ARScript_AnyFromScriptString.name] = anyFromScriptString

    let scriptStringFromAny: ScriptStringFromAny.Function = {
        guard let t = $0 as? T else { throw ARScript_Error.typeMismatch }
        return try scriptStringFrom(t)
    }
    arType.functions[ScriptStringFromAny.name] = scriptStringFromAny
    
    return arType
}

@discardableResult func addFromScriptString(toARType arType: ARTypeT<Void>) -> ARTypeT<Void> {
    return use(
        fromScriptString: { _ in () },
        scriptStringFrom: { _ in "()" },
        forARType: arType)
}

@discardableResult func addFromScriptString<T>(toARType arType: ARTypeT<T>) -> ARTypeT<T>
    where T: AnyObject {
    let fromScriptString: FromScriptString<T>.Function = {
        guard let object = ARObjectRegistry.shared.object(forId: $0) as? T
            else { throw ARScript_Error.objectNotFound(id: $0) }
        return object
    }
    let scriptStringFrom: ScriptStringFrom<T>.Function = {
        let objectId = ARObjectRegistry.shared.id(forObject: $0)
        let className = String(describing: type(of: $0))
        let nativeObjectString = "{ \"arsdk_objectId\": \(objectId.quoted), \"arsdk_className\": \(className.quoted) }"
        return nativeObjectString
    }
    return use(fromScriptString: fromScriptString, scriptStringFrom: scriptStringFrom, forARType: arType)
}

@discardableResult func addFromScriptString<T>(toARType arType: ARTypeT<T>) -> ARTypeT<T>
    where T: Codable {
    assert(!arType.name.hasPrefix("Optional<"),
        "Optional type `\(arType.name)` needs to be manually exported")
    let fromScriptString: FromScriptString<T>.Function = {
        guard let jsonData = $0.data(using: .utf8)
            else { throw ARScript_Error.jsonStringToDataFailed }
        return try JSONDecoder().decode(T.self, from: jsonData)
    }
    let scriptStringFrom: ScriptStringFrom<T>.Function = {
        let jsonData = try JSONEncoder().encode($0)
        guard let jsonString = String(data: jsonData, encoding: .utf8)
            else { throw ARScript_Error.jsonDataToStringFailed }
        return jsonString
    }
    return use(fromScriptString: fromScriptString, scriptStringFrom: scriptStringFrom, forARType: arType)
}


@discardableResult func addFromScriptString<W>(toARType arType_: ARTypeT<W?>) -> ARTypeT<W?> {
    assert(arType(W.self).functions["fromScriptString"] as? FromScriptString<W>.Function != nil)
    let fromScriptString: FromScriptString<W?>.Function = {
        if $0 == "nil" || $0 == "\"arsdk__nil__\"" { return .none }
        return .some(try arType(W.self).from(scriptString: $0))
    }
    let scriptStringFrom: ScriptStringFrom<W?>.Function = {
        switch $0 {
            case .none: return "nil"
            case .some(let wrapped): return try arType(W.self).scriptString(from: wrapped)
        }
    }
    return use(fromScriptString: fromScriptString, scriptStringFrom: scriptStringFrom, forARType: arType_)
}

func result<R>(fromScriptString stringResult: String) throws -> R {
    let resultTypeName = String(describing: R.self)
    guard let resultARType = ARTypeRegistry.shared.find(typeNamed: resultTypeName) as? ARTypeT<R>
        else { throw ARScript_Error.typeNotFound(name: resultTypeName) }
    return try resultARType.from(scriptString: stringResult)
}

@discardableResult func addFromScriptString<R>(
    toARType arType_: ARTypeT<() throws -> R>)
    -> ARTypeT<() throws -> R> {
    
    let fromScriptString: FromScriptString<() throws -> R>.Function = {
        guard let scriptExecutor = gScriptExecutor else { throw ARScript_Error.unexpected }
        let callbackId = $0
        return {
            let stringResult = try scriptExecutor.callback(
                withId: callbackId, params: [])
            return try result(fromScriptString: stringResult)
        }
    }
    let scriptStringFrom: ScriptStringFrom<() throws -> R>.Function = { _ in fatalError() }
    return use(fromScriptString: fromScriptString, scriptStringFrom: scriptStringFrom, forARType: arType_)
}

@discardableResult func addFromScriptString<R, P0>(
    toARType arType_: ARTypeT<(P0) throws -> R>)
    -> ARTypeT<(P0) throws -> R> {
    
    let fromScriptString: FromScriptString<(P0) throws -> R>.Function = {
        guard let scriptExecutor = gScriptExecutor else { throw ARScript_Error.unexpected }
        let callbackId = $0
        return {
            let sp0 = try arType(P0.self).scriptString(from: $0)
            let stringResult = try scriptExecutor.callback(withId: callbackId, params: [sp0])
            return try result(fromScriptString: stringResult)
        }
    }
    let scriptStringFrom: ScriptStringFrom<(P0) throws -> R>.Function = { _ in fatalError() }
    return use(fromScriptString: fromScriptString, scriptStringFrom: scriptStringFrom, forARType: arType_)
}

@discardableResult func addFromScriptString<R, P0, P1>(
    toARType arType_: ARTypeT<(P0, P1) throws -> R>)
    -> ARTypeT<(P0, P1) throws -> R> {
    
    let fromScriptString: FromScriptString<(P0, P1) throws -> R>.Function = {
        guard let scriptExecutor = gScriptExecutor else { throw ARScript_Error.unexpected }
        let callbackId = $0
        return {
            let sp0 = try arType(P0.self).scriptString(from: $0)
            let sp1 = try arType(P1.self).scriptString(from: $1)
            let stringResult = try scriptExecutor.callback(
                withId: callbackId, params: [sp0, sp1])
            return try result(fromScriptString: stringResult)
        }
    }
    let scriptStringFrom: ScriptStringFrom<(P0, P1) throws -> R>.Function = { _ in fatalError() }
    return use(fromScriptString: fromScriptString, scriptStringFrom: scriptStringFrom, forARType: arType_)
}

