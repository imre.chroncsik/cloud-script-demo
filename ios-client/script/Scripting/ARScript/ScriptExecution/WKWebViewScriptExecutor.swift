//
//  WKWebViewScriptExecutor.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 6/3/20.
//  Copyright © 2020 RYOT. All rights reserved.
//

import Foundation
import WebKit

class WKWebViewScriptExecutor: NSObject {
    let language: ARScript_Language = ARScript_JavaScript()
    var interface: String = ""
    var hasFinishedDefiningScriptSideClasses = false
    var pendingExportedObjectsById: [String : ARScript_PendingExportedObject] = [:]

    override init() {
        super.init()
        webView.uiDelegate = self

        do {
            try commonInit()
        } catch {
            fatalError()
        }
    }
    
    private var webView = WKWebView()
    private struct PromptCommandHandler {
        let commandName: String
        let execute: (String) -> String
    }
    private var promptCommandHandlers: [PromptCommandHandler] = []
}

extension WKWebViewScriptExecutor: ARScript_ScriptExecutor {    
    func evaluate(script: String) throws -> String? {
        var result: String? = nil
        var error: Error? = nil
        var finished = false
        webView.evaluateJavaScript(script, completionHandler: {
            r, e in
            result = String(describing: r)
            error = e
            finished = true
        })
        
        while !finished {
            RunLoop.current.run(until: Date() + 0.0001)
        }
        
        if let error = error { print(error) }
        return result
    }

    func exportRPCInterface() throws {
        exportExecuteStr()
    }

    func exportExecuteStr() {
        let script = """
            function arsdk__executeNative_str__(command) {
                //return arsdk_prompt("exec_str__", command)
                return prompt("arsdk__" + command)
            }
            """
        webView.evaluateJavaScript(script)
    }
    
    func exportPlatformId() throws {
        let script = """
            let arsdk_platform = "ios"
            """
        webView.evaluateJavaScript(script)
    }
}

private extension WKWebViewScriptExecutor {
    func executeAndStringifyResult(jsonCallString: String) -> String? {
        do {
            if let resultValue = try execute(nativeCallJsonString: jsonCallString) {
                return ARScript_StringResult(value: resultValue).jsonString
            }
            return nil
        } catch {
            return ARScript_StringResult(error: error).jsonString
        }
    }
    
}

extension WKWebViewScriptExecutor: WKUIDelegate {
    func webView(
        _ webView: WKWebView,
        runJavaScriptTextInputPanelWithPrompt prompt: String,
        defaultText: String?,
        initiatedByFrame frame: WKFrameInfo,
        completionHandler: @escaping (String?) -> Void) {

        let prefix = "arsdk__"
        if prompt.hasPrefix(prefix) {
            let jsonCallString = String(prompt.dropFirst(prefix.count))
            let result = self.executeAndStringifyResult(jsonCallString: jsonCallString)
            completionHandler(result)
        } else {
            //  todo
            completionHandler("")
        }
    }
}
