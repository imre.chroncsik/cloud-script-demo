//
//  ARScriptExecutor.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 5/28/20.
//  Copyright © 2020 RYOT. All rights reserved.
//

import Foundation


protocol ARScript_ScriptExecutor: class {
    func didFinishDefiningScriptSideClasses()
    @discardableResult func evaluate(script: String) throws -> String?
    func export(object: AnyObject, withGlobalName: String?) throws
    @discardableResult func callback(withId: String, params: [String]) throws -> String
    func execute(nativeCallJsonString: String) throws -> String?
    func startScript() throws -> String

    //  private
    var language: ARScript_Language { get }
    var interface: String { get set }
    var hasFinishedDefiningScriptSideClasses: Bool { get set }
    var pendingExportedObjectsById: [String : ARScript_PendingExportedObject] { get set }
    
    func commonInit() throws
    func exportPlatformId() throws
    func exportRPCInterface() throws
    @discardableResult func jsonStringResultForCallback(withCallbackJsonString callbackString: String) throws -> String
    func exportUnchecked(object: AnyObject, withGlobalName globalName: String?) throws
    func exportPendingExportedObjects()
}

struct ARScript_PendingExportedObject {
    weak var object: AnyObject? = nil
    var globalName: String? = nil
}

//  todo remove
var gScriptExecutor: ARScript_ScriptExecutor? = nil

extension ARScript_ScriptExecutor {
    func commonInit() throws {
        try registerMetaClassInstances()
        try exportPlatformId()
        try exportRPCInterface()
        try printInterface()
    }
    
    func didFinishDefiningScriptSideClasses() {
        hasFinishedDefiningScriptSideClasses = true
        exportPendingExportedObjects()
    }
    
    func printInterface() throws {
        interface = try language.interface()
        print(interface)
    }
        
    func registerMetaClassInstances() throws {
        for (typeName, arType) in ARTypeRegistry.shared.typesByName {
            if !arType.isClass { continue }
            let globalObjectName = "arsdk_class_" + typeName
            try ARObjectRegistry.shared.register(object: arType, withId: globalObjectName)
        }
    }

    func export(object: AnyObject, withGlobalName globalName: String?) throws {
        guard hasFinishedDefiningScriptSideClasses else {
            let objectId = ARObjectRegistry.shared.id(forObject: object)
            pendingExportedObjectsById[objectId] = ARScript_PendingExportedObject(
                object: object, globalName: globalName)
            return
        }
    
        try exportUnchecked(object: object, withGlobalName: globalName)
    }

    func exportUnchecked(object: AnyObject, withGlobalName globalName: String?) throws {
        assert(hasFinishedDefiningScriptSideClasses)
        let script = try language.scriptForExporting(object: object, withGlobalName: globalName)
        try evaluate(script: script)
    }

    func exportPendingExportedObjects() {
        for exportedObject in pendingExportedObjectsById.values {
            guard let object = exportedObject.object else { continue }
            do {
                try exportUnchecked(object: object, withGlobalName: exportedObject.globalName)
            } catch {
                print(error)
                fatalError()
            }
        }
    }
        
    @discardableResult func callback(
        withId callbackId: String,
        params: [String])
        throws -> String {
        
        assert(Thread.isMainThread)
        let callback = ARScript_Callback(callbackId: callbackId, params: params)
        let callbackJsonData = try JSONEncoder().encode(callback)
        guard let callbackJsonString = String(data: callbackJsonData, encoding: .utf8)
            else { throw ARScript_Error.jsonDataToStringFailed }
        let jsonStringResult = try self.jsonStringResultForCallback(
            withCallbackJsonString: callbackJsonString)
        let result = ARScript_StringResult(jsonString: jsonStringResult)
        if let error = result.error { throw ARScript_Error.scriptException(description: error) }
        if let value = result.value { return value }
        return "()"
    }
    
    @discardableResult func jsonStringResultForCallback(
        withCallbackJsonString callbackString: String)
        throws -> String {
        
        let script = try language.scriptForExecutingCallback(withCallbackJsonString: callbackString)
        guard let result = try evaluate(script: script) else { throw ARScript_Error.unexpected }
        return result
    }

    func execute(nativeCallJsonString: String) throws -> String? {
        assert(Thread.isMainThread)
        return try ARScript_CallStringExecutor.shared.execute(jsonCallString: nativeCallJsonString)
    }
    
    func startScript() throws -> String {
        return try callback(withId: "start", params: [])
    }
}

fileprivate struct ARScript_Callback: Codable {
    let callbackId: String
    let params: [String]
}

