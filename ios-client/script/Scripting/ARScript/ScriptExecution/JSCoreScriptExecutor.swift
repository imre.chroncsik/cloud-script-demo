//
//  JSCoreScriptExecutor.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 6/2/20.
//  Copyright © 2020 RYOT. All rights reserved.
//

import Foundation

import JavaScriptCore

class JSCoreScriptExecutor {
    let language: ARScript_Language = ARScript_JavaScript()
    var interface: String  = ""
    var hasFinishedDefiningScriptSideClasses = false
    var pendingExportedObjectsById: [String : ARScript_PendingExportedObject] = [:]
    var lastException: JSValue? = nil

    init?() {
        context = JSContext()
        context.exceptionHandler = { _, x in self.lastException = x }
        try? commonInit()
        exportJSCorePrint()
    }

    private let context: JSContext
}

extension JSCoreScriptExecutor: ARScript_ScriptExecutor {    
    @discardableResult func evaluate(script: String) throws -> String? {
        lastException = nil
        let result = context.evaluateScript(script)?.toString()
        if let exception = lastException {
            print(exception.toString() ?? "unknown js exception")
        }
        return result
    }

    func exportRPCInterface() throws {
        exportExecuteStr()
    }

    func exportExecuteStr() {
        let arsdk_executeNative_str: @convention(block) (String) -> String? = {
            [weak self] (jsonCallString: String) -> String? in
            guard let self = self else { return ARScript_StringResult(error: ARScript_Error.unexpected).jsonString }
            do {
                if let resultValueString = try self.execute(nativeCallJsonString: jsonCallString) {
                    let resultJsonString = ARScript_StringResult(value: resultValueString).jsonString
                    return resultJsonString
                }
                fatalError()   // now always expecting a result, even for void-returning functions
                return nil
            } catch {
                if let error = error as? ARScript_Error {
                    print("script error")
                    print(error)
                }
                return ARScript_StringResult(error: error).jsonString
            }
        }
        context.setObject(arsdk_executeNative_str, forKeyedSubscript: "arsdk__executeNative_str__" as NSString)
    }
    
    func exportPlatformId() throws {
        context.setObject("ios", forKeyedSubscript: "arsdk_platform" as NSString)
    }
}

private extension JSCoreScriptExecutor {
    func exportJSCorePrint() {
        let arsdk_print: @convention(block) (String) -> Void  = {
            (str: String) -> Void in
            print(str)
        }
        context.setObject(arsdk_print, forKeyedSubscript: "arsdk_jsCore_print" as NSString)
    }
}

