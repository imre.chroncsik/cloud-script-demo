//
//  RemoteScriptMessage.swift
//  script
//
//  Created by Imre Chroncsik on 6/26/20.
//  Copyright © 2020 Ryot. All rights reserved.
//

import Foundation

struct ScriptMessage: Codable {
    var messageId: Int
    var respondingToMessageId: Int? = nil   //  if it's a response to a query, what was the query's messageId?

    var error: String? = nil
    var value: String? = nil
    
    var nativeCall: String? = nil
    var scriptCallback: String? = nil

    var exportObjectId: String? = nil
    var exportObjectClassName: String? = nil
    var exportObjectGlobalVarName: String? = nil
    
    init() {
        messageId = ScriptMessage.nextMessageId
        ScriptMessage.nextMessageId += 1
    }
        
    private static var nextMessageId = 0
}
