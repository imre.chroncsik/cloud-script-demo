//
//  ARScriptServer.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 6/5/20.
//  Copyright © 2020 RYOT. All rights reserved.
//

/*
import Foundation
import UIKit

//  implement the RemoteScriptExecutorDependencies interface in terms of an ARSocketServer
class RemoteScriptExecutorDependencies_ARSocketServer {
    enum Error: Swift.Error {
        case noConnection
    }

    init(server: ARSocketServer) {
        self.server = server
    }
    
    //  private
    let server: ARSocketServer
}

extension RemoteScriptExecutorDependencies_ARSocketServer: RemoteScriptExecutorDependencies {
    var isConnected: Bool { server.connection != nil }
    
    func send(string: String) throws {
        guard let connection = server.connection
            else { throw Error.noConnection }
        connection.send(string: string, closeWhenFinished: false)
    }
    
    func send(data: Data) throws {
        guard let connection = server.connection
            else { throw Error.noConnection }
        return connection.send(data: data, closeWhenFinished: false)
    }
}

*/
