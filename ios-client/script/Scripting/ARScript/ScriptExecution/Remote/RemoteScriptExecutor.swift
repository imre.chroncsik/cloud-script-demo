//
//  RemoteScriptExecutor.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 6/18/20.
//  Copyright © 2020 RYOT. All rights reserved.
//

import Foundation

extension RemoteScriptExecutor: ARScript_ScriptExecutor {}

protocol RemoteScriptExecutorDependencies: class {
    var isConnected: Bool { get }
    func send(string: String) throws
    func send(data: Data) throws
}

class RemoteScriptExecutor {
    weak var dependencies: RemoteScriptExecutorDependencies!

    enum Error: Swift.Error {
        case noDependencies
    }

    var language: ARScript_Language
    var interface = ""
    var hasFinishedDefiningScriptSideClasses = false
    
    init(dependencies: RemoteScriptExecutorDependencies, language: ARScript_Language) {
        self.dependencies = dependencies
        self.language = language
        try? registerMetaClassInstances()
    }
    
    func didConnect() {
        DispatchQueue.main.async {
            [weak self] in
            guard let self = self else { return }
            self.didFinishDefiningScriptSideClasses()
            do {
                _ = try self.startScript()
            } catch {
                print("startScript() error: ", error)
                fatalError()
            }
        }
    }
    
    func didReceive(message: String) {
        parseMessages(fromString: message)
    }
        
    //  private
    
    var pendingExportedObjectsById: [String : ARScript_PendingExportedObject] = [:]

    var sharedMessage: ScriptMessage? = nil
    let didWriteSharedMessage = DispatchSemaphore(value: 0)
    let didReadSharedMessage = DispatchSemaphore(value: 0)
    
}

extension RemoteScriptExecutor {
    //  just send message, don't wait for a response
    func send(asyncMessage: ScriptMessage) throws {
        guard let dependencies = dependencies else { throw Error.noDependencies }
        let messageJsonData = try JSONEncoder().encode(asyncMessage)
        try dependencies.send(data: messageJsonData)
    }

    //  send a message, wait for a response
    func send(syncMessage: ScriptMessage) throws -> ScriptMessage {
        try send(asyncMessage: syncMessage)
        let responseMessage = try getResponse(forMessageId: syncMessage.messageId)
        return responseMessage
    }

    func evaluate(script: String) throws -> String? {
        throw ARScript_Error.notSupported
    }

    /*
        execute a script-side callback represented by `callbackString`,
        return the result as a String encoding a JSON object
        encoding an ARScript_StringResult (value / error as strings).
    */
    @discardableResult func jsonStringResultForCallback(
        withCallbackJsonString callbackString: String)
        throws -> String {
                
        var message = ScriptMessage()
        message.scriptCallback = callbackString
        let responseMessage = try send(syncMessage: message)
        let resultString = try self.resultString(fromResponseMessage: responseMessage)
        return ARScript_StringResult(value: resultString).jsonString
    }

    func resultString(fromResponseMessage responseMessage: ScriptMessage) throws -> String {
        if let scriptError = responseMessage.error {
            throw ARScript_Error.scriptException(description: scriptError) }
        guard let resultString = responseMessage.value else { throw ARScript_Error.unexpected }
        return resultString
    }

    func exportUnchecked(object: AnyObject, withGlobalName globalName: String?) throws {
        assert(dependencies.isConnected)
        var message = ScriptMessage()
        message.exportObjectId = ARObjectRegistry.shared.id(forObject: object)
        message.exportObjectClassName = String(describing: type(of: object))
        message.exportObjectGlobalVarName = globalName
        
        let responseMessage = try send(syncMessage: message)
        let resultString = try self.resultString(fromResponseMessage: responseMessage)
        guard resultString == "ack" else { throw ARScript_Error.unexpected }
    }

    func exportRPCInterface() throws {
        //  when remote scripting, the rpc interface (the executeNative_str() function)
        //  is implemented fully on the script side, see remote-bootstrap.ts.
    }
    
    func exportPlatformId() throws {
        //  this is also handled on the script side, see remote-bootstrap.ts. 
    }
}

//
extension RemoteScriptExecutor {

    func parseMessages(fromData data: Data) {
        assert(!Thread.isMainThread)
        guard let messagesString = String(data: data, encoding: .utf8) else { return }
        parseMessages(fromString: messagesString)
    }
    
    func parseMessages(fromString messagesString: String) {
        assert(!Thread.isMainThread)
        let messageStrings = messagesString.components(separatedBy: "###arsdk-message-separator###")
        for messageString in messageStrings {
            if messageString.isEmpty { continue }
            guard let messageData = messageString.data(using: .utf8) else { continue }

            var maybeMessage: ScriptMessage? = nil
            do {
                maybeMessage = try JSONDecoder().decode(ScriptMessage.self, from: messageData)
            } catch {
                //  todo report error -- to whom? delegate / dependencies?
                print(error)
                return
            }
            
            guard let message = maybeMessage else { continue }
            sharedMessage = message
            DispatchQueue.main.async { [weak self] in self?.processSharedMessage() }
            didWriteSharedMessage.signal()
            while (sharedMessage != nil) {
                didReadSharedMessage.wait()
            }
        }
    }
    
    func processSharedMessage() {
        assert(Thread.isMainThread)
        guard let message = readSharedMessage() else { return }
        process(message: message)
    }
    
    func readSharedMessage() -> ScriptMessage? {
        let message = sharedMessage
        sharedMessage = nil
        didReadSharedMessage.signal()
        return message
    }

    func getResponse(forMessageId queryMessageId: Int) throws -> ScriptMessage {
        assert(Thread.isMainThread)
        while true {
            while (sharedMessage == nil) {
                didWriteSharedMessage.wait()
            }
            guard let message = readSharedMessage() else { throw ARScript_Error.unexpected }
            if message.respondingToMessageId == queryMessageId {
                return message
            }
            process(message: message)
        }
    }

    func process(message: ScriptMessage) {
        assert(Thread.isMainThread)
    
        //  response messages should areadly have been handled in getResponse(forMessageId:)
        assert(message.respondingToMessageId == nil)
        let maybeResponse = self.processAndRespondTo(message: message)
        if let response = maybeResponse {
            //  todo handle exception
            try? self.send(asyncMessage: response)
        }
    }
    
    func processAndRespondTo(message: ScriptMessage) -> ScriptMessage? {
        if let nativeCallJsonString = message.nativeCall {
            return processAndRespondTo(
                nativeCallJsonString: nativeCallJsonString,
                queryMessageId: message.messageId)
        }
    
        fatalError()
    }
    
    func processAndRespondTo(nativeCallJsonString: String, queryMessageId: Int?) -> ScriptMessage? {
        var response = ScriptMessage()
        response.respondingToMessageId = queryMessageId
        
        do {
            response.value = try execute(nativeCallJsonString: nativeCallJsonString)
            if response.value == nil {
                assert(false)
                response.value = "()"
            }
        } catch {
            print(nativeCallJsonString)
            response.error = String(describing: error)
        }
        return response
    }

}
