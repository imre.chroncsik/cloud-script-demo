//
//  ScriptClient.swift
//  script
//
//  Created by Imre Chroncsik on 10/14/20.
//  Copyright © 2020 Ryot. All rights reserved.
//

import Foundation
import Starscream

//  todo move some parts out into a Starscream-agnostic RemoteScriptClient protocol / base class. 
class RemoteScriptStarscreamClient {
    var didConnect: (() -> Void)? = nil
    var didReceiveMessage: ((String) -> Void)? = nil
    
    var isConnected: Bool = false

    init(url: URL) {
        var request = URLRequest(url: url)
        request.timeoutInterval = 5
        socket = WebSocket(request: request)
        socket.onEvent = { [weak self] event in self?.didReceive(event: event) }
        socket.callbackQueue = socketQueue
        socket.connect()
    }
    
    //  private
    private var socketQueue = DispatchQueue(label: "RemoteScriptStarscreamClient.socketQueue")
    private var socket: WebSocket
    
    private func didReceive(event: WebSocketEvent) {
    
        switch event {
            case .error(let error):
                isConnected = false
                print("ScriptClient.didReceive.error: ", error ?? "unknown")
                
            case .connected(let headers):
                isConnected = true
                print("websocket is connected: \(headers)")
                didConnect?()
                
            case .disconnected(let reason, let code):
                print("disconnected, reason = \(reason), code = \(code)")
                isConnected = false
                
            case .cancelled:
                isConnected = false

            case .ping(_):
                break
            case .pong(_):
                break
            case .viabilityChanged(let viable):
                print(viable)
                break
            case .reconnectSuggested(let b):
                print(b)
                break
        
            case .text(let string):
                didReceiveMessage?(string)
                
            default: break
        }
    }
}

extension RemoteScriptStarscreamClient: RemoteScriptExecutorDependencies {
    func send(string: String) throws { socket.write(string: string) }
    func send(data: Data) throws { socket.write(data: data) }
}


