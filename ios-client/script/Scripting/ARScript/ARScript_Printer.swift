//
//  ScriptPrinter.swift
//  script
//
//  Created by Imre Chroncsik on 6/5/20.
//  Copyright © 2020 Ryot. All rights reserved.
//

import Foundation

class ARScript_Printer {
    static func exportMethods() {
        ar_export1(staticMethod: ARScript_Printer.print_, ofClass: ARScript_Printer.self, named: "print")
    }

    static func print_(_ string: String) {
        print(string)
    }
}
