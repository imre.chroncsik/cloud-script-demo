//
//  CallStringExecutor.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 5/28/20.
//  Copyright © 2020 RYOT. All rights reserved.
//

import Foundation

/*
    Receives a JSON string, decodes it into an object that describes a method call,
    executes it, returns the result as a string.
*/
class ARScript_CallStringExecutor {
    static let shared = ARScript_CallStringExecutor()

    func execute(jsonCallString: String) throws -> String? {
        struct Call: Codable {
            let objectId: String
            let methodName: String
            let parameters: [String]
        }
        guard let jsonCallData = jsonCallString.data(using: .utf8)
            else { throw ARScript_Error.jsonStringToDataFailed }
        let call = try JSONDecoder().decode(Call.self, from: jsonCallData)
        
        guard let object = ARObjectRegistry.shared.object(forId: call.objectId)
            else { throw ARScript_Error.objectNotFound(id: call.objectId) }
        let typeName = String(describing: type(of: object))
        guard let objectType = ARTypeRegistry.shared.find(typeNamed: typeName)
            else { throw ARScript_Error.typeNotFound(name: String(describing: type(of: object))) }
        guard let method = objectType.methodsByName[call.methodName]
            else { throw ARScript_Error.methodNotFound(name: "\(objectType.name).\(call.methodName)") }

        let anyParams = try self.anyParams(fromStringParams: call.parameters, forMethod: method)
        let anyResult: Any = try method.anyCall(object: object, params: anyParams)
        let stringResult = try method.resultType.scriptString(fromAny: anyResult)
        return stringResult
    }
    
    func anyParams(
        fromStringParams stringParams: [String],
        forMethod method: ARMethod)
        throws -> [Any] {
        
        guard method.signature.count == stringParams.count + 2
            else { throw ARScript_Error.paramCountMismatch }
        var anyParams: [Any] = []
        for i in 0 ..< stringParams.count {
            let paramType = method.signature[i + 2]
            let anyParam: Any = try paramType.any(fromScriptString: stringParams[i])
            anyParams.append(anyParam)
        }
        return anyParams
    }
}
