//
//  ARScriptEntity.swift
//  ARSDK
//
//  Created by Imre Chroncsik on 7/8/20.
//

import Foundation

class ARScriptEntity: Entity {
    var hasRequiredAssets = true
    
    let sourceAsset: FetchableAsset
    
    init(sourceAsset: FetchableAsset) {
        self.sourceAsset = sourceAsset
    }
}
