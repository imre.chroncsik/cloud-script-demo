//
//  ViewController.swift
//  script
//
//  Created by Imre Chroncsik on 5/26/20.
//  Copyright © 2020 Ryot. All rights reserved.
//

import UIKit
import QRCodeReader

/*
        todo

    *   BUG: if we send a non-query message, and an error happens on the script side,
        then script tries to send to error back, but native won't ever read / process it.
        because the error message has a respondingToMessageId field,
        native won't process it as a script-initiated call,
        but rather will treat it as a response to a native -> script call,
        so place it in pendingMessages, assuming that getResponse(forMessageId:) expects it.
        but if the original native -> script call had a void return type,
        then there's no getResponse().
        todo check how this works on the syncified script side.
        the whole pendingMessages thing just shouldn't exist here.
        i suspect that it's a way to avoid blocking the main trhead,
        but actually blocking the main thread while waiting for the result of a n2s call
        is perfectly fine (as long as we can still dispatch / exceute incoming s2n calls).
    *   callbacks vs main / bg threads
        now callbacks can't be called on main, bc then the main thread woudl be blocked waiting for a response.
        so before doing a callback we do dispatch.global.async.
        but that's not a good idea, now things like pendingMessages are accessed from multiple queues.
        probably we need a dedicated queue.
        but.
        some callbacks may have a return value,
        and the native code calling those callbacks probably assumes them to be sync calls.
        and of course those native pieces may get called on the main thread.
        how the hell will we handle this?
        first of all, we need a non-main thread for receiving messages. (SocketConn.start => nwConn.start(queue:))
        then we send out the callback on main and do block,
        but if the script calls a native func from within the callback's body,
        then that call to native will happen on a bg thread .
        but this still doesn't really help much,
        because the native func called from the script-side callback may try to dispatch to main.
        probably the only way to handle this is to do some kind of active waiting in RemoteExec.callback(),
        like in WKWebViewExecutor.eval().
        dispatch the call to script to bg, on main wait for a semaphore,
        periodically call RunLoop.current.run(mode: .common, date: ...).
        except that that doesn't work for some reason.
        NO, forget most of the above.
        if a blocking native -> script function is called on main, then yes, block the main thread.
        for production this should be fine, and for script debugging, blocking is kinda fine.
        the only issue is that even whiel blocking on main, we still need to be able to
        receive and dispatch incoming network messages.
        hm, i did have issues with this in the past, where just pumping the main queue didn't work as i expected.
        what was that exactly?
    *   use two different sockets for script-initiated and native-initiated comm.
        while setup is a bit more complicated, this should remove lots of complexity while running.
        now, with a single socket, we may run into situations where eg. the script calls a native func,
        but instead of receiving a result, it first receives a callback.
        with two sockets these should be decoupled: func result is coming back on the script-initiated socket,
        while the callback is coming in on the native-initiated socket.
        the script would need to async-listen on the native-initiated socket (the script is a server here),
        but it coudl write into and then sync-wait for result on the script-initiated socket.
        so probably there would be no need for caching pending responses on the script side. (am i 100% sure?)
    *   callbacks should be able to return errors; now they can only return a value.
        just like execNative_str returns a stringified json with either a value or an error field,
        execCallback should also return value / error. 
    +?   remove JavaScriptExecutor. only keep JSCore / WKWebView / Remote as executors,
        JS-specific shared code is in JSIntfGen (possibly rename the latter?)
    *   callback vs script-to-swift call collisions? deadlocks?
        script does a to-native call, sync waits for result.
        at the same time the native app does a callback (eg. setInterval()),
            to a callback func that has a return value, so native now waits for a result.
        what happens?
        can we just disable non-void callbacks, at least for now?
            then the app could just send the callback async, without waiting for a result.
        but even then, there might be trouble on the receiving end.
        script sent out a call, started waitig for a response, but received a callback (or other eval) instead.
        i think we have two options:
        a)  async-await. requires marking just about every function in the user script in async.
            then again, if we want remote scripting with an online js ide on one side, then this may be the only option.
        b)  create an external non-js app for receiving remote messages from the arsdk app, call this ext app using spawnsync.
        we'd need (for the ext receiver, but some of it for async-await too)
        *   message ids -- for async-await too.
        *   some kind of a message buffer, so we can store the callback req if comes in while waiting for a call result.
            we can't do this inside js, needs threads.
        *   some kind of an external program that would receive from the remote app and buffer,
            and a second external program that coudl query the other one for a new message in the buffer,
            either with a specific callId (and in this case block until that arrives),
            or just to check if there's a new message with no callId (callback / eval),
            but in either case it would exit once it finished with such a single query (so it can be used with spawnSync).
            i guess the ext buffering app would need a different socket to the server, right? can't share with the js script.
            maybe publish two diff services from the script server? arscript_tonative, arscript_fromnative?
            js would connect to tonative, the response buffering app woudl connect to fromnative.
            arscript_receive host port
            arscript_
    *   native -> js callbacks shouldn't _always_ wait for a response from the script side;
        if it's a void callback, then just send it out, return immediately. 
    *   remove `closeWhenFinished: true` in ScriptServer (reqs a smarter netcat), see if that speeds things up.
    *   the export() vs export1() diff may be fixed by changing the signatures
        eg. export(_: ((P0) -> R).Type) => export(_: (((P0)) -> R).Type)
        the double parens around P0 decompose the tuple type, and now it only matches a tuple with one element.
        (the problem with single-paren (P0) -> R is that it matches any number of parameters,
        assuming P0 is a tuple of N values.)
        duh, again, i vaguely remember that this actually doesn't work, but can't remember why...
    *   Scripting.exportedTypes; only export() puts types in, not register();
        IntfGenerator should only iterate these.
    *   separate platformInterface from exportedInterface
        ???
    *   test passing objects from script to swift as parameter.
        probably we should have FromString<AnyObject> -- except we can't extend AnyObject...
        and creating a fromString() overload for AnyObject may also be tricky,
        because everythign is an AnyObject.
    *   addFromScriptString variants: remove void / non-void branching if possible,
        not trivial though.
        when calling a script callback, we'll need to convert the string result (produced by scrpt)
        into a swift typed value. for that we need a fromString func in ARType.
        if the callback is void, then we need ARTypeT<Void>.functions["fromString"].
        but who would reg that?
        if we required that return types have fromString (either InitFromString or manually regged), then we'd be fine.
    *   remove arType() (use ARType() construcotr instead.)
        once we remove string stuff, and in general delegate registering type functions to addTypeFunc()s
        and wrappers like export(), we shouldn't need any specializations for arType, therefore no need for it
        to be a function.
    *   ARProp: remove InitFromStr constraint (move that to script export(prop:)
        add support for explicitly specified types (so client code can wrap with addFromStr)
    *   documentation about export() vs export1()
        (if only one param, then that single P0 type param can be inferred as a tuple,
        thus matching function types of any arity)
    *   callbacks
        *   only implemented for 1-param, also impl for 0, 2.
        *   add support for passing functions to class methods. (need to pass signature [ARType])
        *   optional types? for setCallback() we don't need it, but if `callback` is an exposed property, then yes.
    *   decouple fromString<FuncType>() from gScriptExecutor.
    *   scriptExec.regMetaclass => stop putting things like arsdk_class_Int.Type or arsdk_class_().Type into the map.
    *   calling superclass methods (derived.baseMethod() -- won't find in the Derived method table)
    *   use TypeId instead of typename (ofc it's the same under hte hood but still)
    *   type support
        *   passing structs -- json?
        *   use Codable conformance instead of ARFromString. (or both?)
    *   implement setInterval()
    +   export class properties (now can export as getter / setter func, should be a prop on the js side too)
    *   exceptions js -> swift
        *   scriptExec() should return a Result enum with value / error cases
        *   always auto-wrap js code in try / catch, returns exceptions as Result.error
    *   reg(X.foo) instead of reg(X.class, { $0.foo($1) })
    *   remote scripting
        *   ScriptServer -- receive callScripts, execute them, send back result
        *   RemoteScriptExecutor -- handles calling script from swift.
            probably we can restrict this to callbacks:
            it's the script that subscribes, passing along a generated callback id,
            RemoteScriptExecutor only (all ScriptExecutors, actually) only supports
            calling a callback by id.
            as for _how_ the script subscribes, best would be to support function-typed properties.
            the script woudl create an observer object, set its onEvent = function() { ... }
    *   support multiple init()s. in js that probably means using the max number of params,
        and at call time count how many !== undefined, make the call accordingly.
        *   in general, add support for method overloading.
            probably methodsByName should be methodsByName: [String: [Method]].
    *   add method param and return types as comments to the js interface
    *   handle optional returns
    *   test cases:
        *   print
        *   create instance of swift class from script
        *   call instance method from script
        *   call static method
        *   export objects that were created on the swift side
    *   instead of uuids, use an int counter for generating objIds,
        then use a simple int-indexed array for lookup
        (keep the string -> string map only for objects with custom name-ids, eg. metaclass instances id'd by the class name)
    *   make arsdk_print() take rest params (now it only sends its first param to native)
    *   replace arType() with ARType() (normal obj construction instead of the free function).
        note in this case ARType would be a transparent handle,
        so multiple instances of ARType<T> with the same T woudl refer to the same
        underlying type object (ARTypeImpl<T> or whatever) stored in the type registry.
        in the type registry there would still be only one object for each type.
    *   turn reg() from free func into instance method (on ARReflection?)
    *   impl ARFuncType (as a subtype of ARType) with a signature: [ARType] field,
        make ARMethod ref to a ARFuncType instead of storing its sig.
        this would make it possible to use functions-passed-as-params in a recursive way,
        that is, handle a method that takes a closure that takes another closure etc.
    *   ARType.isClass is a bit hacky now, would be better to base it AnyObject conformance.
        on the other hand, we don't want create even more overloads for the generic functions.
*/


enum XError: Error { case unknownXError }



class X {
    static func exportMethods() {
        ar_export(staticMethod: X.staticFoo, ofClass: X.self, named: "staticFoo")
        ar_export1(staticMethod: X.staticWithParam, ofClass: X.self, named: "staticWithParam")

        ar_export(initMethod: X.init)

        ar_export(method: X.fp2, named: "fp2")
        ar_export(method: X.foo, named: "foo")
        ar_export(method: X.thrower, named: "thrower")

        ar_export1(method: X.setCallback, named: "setCallback",
            returnAndParameterTypes: [
                ar_export(type: Void.self),
                ar_export(type: ((Int) throws -> Void).self) ])
        ar_export(method: X.callCallback, named: "callCallback")

        ar_export1(method: X.setCallback2, named: "setCallback2",
            returnAndParameterTypes: [
                ar_export(type: Void.self),
                ar_export(type: ((X) throws -> Void).self, returnAndParameterTypes: [
                    ar_export(type: Void.self),
                    ar_export(type: X.self) ]) ])
        ar_export(method: X.callCallback2, named: "callCallback2")

        ar_export1(method: X.takesOptional, named: "takesOptional"
//            )   //  should result in an assert failure, optional types must be manually exported
            , returnAndParameterTypes: [ar_export(type: Void.self), ar_exportOptional(type: Int.self)])
        ar_export(method: X.returnInt, named: "returnInt")
        ar_export(method: X.returnString, named: "returnString")

        ar_register(propertyOfClass: X.self, named: "i", setter: { $0.i = $1 }, getter: { $0.i })
        ar_register(propertyOfClass: X.self, named: "readOnly") { $0.readOnly }
        
        ar_export(method: X.createY, named: "createY", returnType: ar_export(type: Y.self))
    }
    
    static func staticFoo() -> String { "staticFoo" }
    static func staticWithParam(p0: Int) -> Int { p0 }

    required init() {}
    deinit {
        print("X.deinit")
    }
    
    var i: Int = 42
    var readOnly: Int { 43 }
    func foo() {
        i += 1; print("X.foo")
        
    }
    func fp2(p0: Int, p1: Int) -> Int { return i + p0 + p1 }
    func thrower() throws { throw XError.unknownXError }
    func takesOptional(i: Int?) { print("got optional int: ", i ?? "nil") }
    func returnInt() -> Int { return 123 }
    func returnString() -> String { return "123" }

    func setCallback(_ callback: @escaping (Int) throws -> Void) {
        print("setCallback")
        self.callback = callback
        try? callback(1000)
        print("setCallback 2")
    }
    private var callback: ((Int) throws -> Void)? = nil
    func callCallback() {
        try? callback?(1001)
    }

    func setCallback2(_ callback: @escaping (X) throws -> Void) {
        self.callback2 = callback
    }
    private var callback2: ((X) throws -> Void)? = nil
    func callCallback2() {
        try? callback2?(self)
    }

    func createY() -> Y { return Y(i: 123) }
}

class Y {
    static func exportMethods() {
        ar_export1(initMethod: Y.init)

        //  export non-existent "methods" using blocks
        ar_export(methodOfClass: Y.self, named: "i",
            returnType: ar_export(type: Int.self))
            { $0.i }
        ar_export1(methodOfClass: Y.self, named: "set_i",
            returnAndParameterTypes: [ar_export(type: Void.self), ar_export(type: Int.self)])
            { $0.i = $1 }
            
        ar_export1(method: Y.takesCodable, named: "takesCodable")
        ar_export(method: Y.yfoo, named: "yfoo")
    }

    required init(i: Int) { self.i = i }
    var i: Int
    func takesCodable(c: MyCodable) { print("takesCodable(): ", c) }
    func yfoo() -> String { "yfoo" }
}

struct MyCodable: Codable {
    let x: Int
    let y: Int
}

class ViewController: UIViewController {
       
    required init?(coder aDecoder: NSCoder) {
        let qrScannerBuilder = QRCodeReaderViewControllerBuilder {
            builder in
            builder.showTorchButton = false
            builder.showSwitchCameraButton = false
            builder.showOverlayView = true
        }
        qrScannerVC = QRCodeReaderViewController(builder: qrScannerBuilder)

        super.init(coder: aDecoder)

        qrScannerVC.modalPresentationStyle = .formSheet
        qrScannerVC.completionBlock = { [weak self] result in self?.didFinishScanningQR(withResult: result) }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        print(UIDevice.current.networkAddress(forInterface: "en0") ?? "---")

        ARScript_Printer.exportMethods()
        X.exportMethods()
        Y.exportMethods()

        let runLocally = false
        if runLocally {
            executeScriptLocally()
        } else {
//            let runAsServer = false
//            runAsServer ? startScriptServer() : connectToScriptServer()


//            connectToScriptServer()
//            try? scriptExecutor?.export(object: nativeX, withGlobalName: "nativeX")
        }
    }
    
    
    //  private

    private var scriptExecutor: ARScript_ScriptExecutor? = nil
    private var scriptExecutorDependencies: RemoteScriptExecutorDependencies? = nil
    
    @IBOutlet private var statusLabel: UILabel!
    private var qrScannerVC: QRCodeReaderViewController
    
    
//    var scriptServer: ScriptServer? = nil
    private let nativeX = X()

    private func executeScriptLocally() {
        scriptExecutor = JSCoreScriptExecutor()
//        scriptExecutor = WKWebViewScriptExecutor()

        if scriptExecutor == nil {
            print("failed to create the script executor")
            fatalError()
        }
        
        gScriptExecutor = scriptExecutor
        
        try? scriptExecutor?.export(object: nativeX, withGlobalName: "nativeX")
        
        let startTime = Date()
        let jsString = #"""
            function arMain() {
                try {
                                
                    nativeX.foo()
                                
                    arsdk_print(".1")
                    arsdk_print(X.staticFoo())
                    arsdk_print(X.staticWithParam(42))
                    arsdk_print(".2")

                    var xx = new X()
                    arsdk_print(".3")
                    arsdk_print(xx.returnInt())
                    arsdk_print(xx.returnString())

                    xx.setCallback2((cbx) => {
                        arsdk_jsCore_print("cbx: ")
                        arsdk_jsCore_print(JSON.stringify(cbx))
                        cbx.foo()
                    })
                    arsdk_print(".3.1")
                    xx.callCallback2()
                    arsdk_print(".3.2")

                    var yFromX = xx.createY()
                    arsdk_print(".4")
                    arsdk_print(yFromX.objectId)
                    arsdk_print(yFromX.yfoo())
                    arsdk_print(".5")
                    
                    xx.foo()
                    arsdk_print("xx.i: ")
                    arsdk_print(xx.i)
                    xx.i = 1
                    arsdk_print(xx.i)
                    arsdk_print("--")
                    
                    arsdk_print("testing callback")
                    xx.setCallback((i) => {
                        arsdk_print(`callback called ${i}`)
                    })
                    xx.callCallback()
                    arsdk_print("end testing callback")
                    
                    arsdk_print("testing: call swift func that takes an optional")
                    xx.takesOptional(42)
                    xx.takesOptional(null)
                    arsdk_print("finished testing: call swift func that takes an optional")

                    var x = 1
                    var y = 2
                    var r = ""
                    for (var i = 0; i < 10000; ++i) {
                        r = xx.fp2(i, y)
                    }
                    
                    var yy = new Y(42)
                    arsdk_print(yy.i())
                    yy.set_i(43)
                    arsdk_print(yy.i())
                    yy.takesCodable({ "x": 1, "y": 2 })
                    
                    //setInterval(1000, () => { arsdk_print("tick") })

                    r
                } catch(x) {
                    arsdk_print("exception:")
                    arsdk_print(x.toString())
                }
            }
        """#
        let result: String? = try? scriptExecutor?.evaluate(script: jsString)
        print("result = ", result ?? "nil")
        
        do {
//            let r2 = try scriptExecutor?.callback(withId: "start", params: [])
            let r2 = scriptExecutor?.startScript()
            print("r2 = ", r2 ?? "nil")
        } catch {
            print(error)
            print("x")
        }

        let endTime = Date()
        print(endTime.timeIntervalSince(startTime))
        print("end")

    }

/*
    func getIPAddress() -> String {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }

                guard let interface = ptr?.pointee else { return "" }
                let addrFamily = interface.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET)
//                    || addrFamily == UInt8(AF_INET6)
                    {

                    // wifi = ["en0"]
                    // wired = ["en2", "en3", "en4"]
                    // cellular = ["pdp_ip0","pdp_ip1","pdp_ip2","pdp_ip3"]

                    let name: String = String(cString: (interface.ifa_name))
                    if  name == "en0" || name == "en2" || name == "en3" || name == "en4" || name == "pdp_ip0" || name == "pdp_ip1" || name == "pdp_ip2" || name == "pdp_ip3" {
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface.ifa_addr, socklen_t((interface.ifa_addr.pointee.sa_len)), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address ?? ""
    }
    */

/*
    func startScriptServer() {
        let server = ARSocketServerImplementation(publishedServiceType: "_arscript._tcp.", publishedServiceName: "arscript")
        
        let scriptExecutorDependencies = RemoteScriptExecutorDependencies_ARSocketServer(server: server)
        self.scriptExecutorDependencies = scriptExecutorDependencies
        
        let scriptExecutor = RemoteScriptExecutor(
            dependencies: scriptExecutorDependencies,
            language: ARScript_JavaScript())
        self.scriptExecutor = scriptExecutor
        gScriptExecutor = scriptExecutor

        server.start()
    }
    */
    
    private func connectToScriptServer(atUrlString urlString: String) {
//        guard let url = URL(
//            string: "https://2897-e93adff5-4d25-49d3-8186-db919a92f432.ws-eu01.gitpod.io")
//            else { fatalError() }
        guard let url = URL(string: urlString) else { fatalError() }
        let starscreamClient = RemoteScriptStarscreamClient(url: url)
        self.scriptExecutorDependencies = starscreamClient
        
        let scriptExecutor = RemoteScriptExecutor(
            dependencies: starscreamClient,
            language: ARScript_JavaScript())
        self.scriptExecutor = scriptExecutor
        gScriptExecutor = scriptExecutor
        
        starscreamClient.didConnect = {
            [weak scriptExecutor, weak self] in
            DispatchQueue.main.sync {
                self?.statusLabel.text = "Connected"                
            }
            scriptExecutor?.didConnect()
        }
        starscreamClient.didReceiveMessage = {
            [weak scriptExecutor] message in
            scriptExecutor?.didReceive(message: message)
        }
    }

    @IBAction private func didTapConnectByScanningQR() {
        present(qrScannerVC, animated: true)
    }
    
    @IBAction private func didTapRestart() {
        scriptExecutor?.startScript()
    }
    
    private func didFinishScanningQR(withResult maybeQrResult: QRCodeReaderResult?) {
        guard let qrResult = maybeQrResult else { return }
        let url = qrResult.value
        dismiss(animated: true, completion: {
            [weak self] in
            guard let self = self else { return }
            self.statusLabel.text = "Connecting..."
            self.connectToScriptServer(atUrlString: url)
        })
    }

}

