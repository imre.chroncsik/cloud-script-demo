
*	
	*	arsdk_execute()
	*	arsdk_print()? may just return the promise from exec()
	*	executeCallback
	*	Class.init
	*	arMain()
	*	setCallback(async () => ...

----

	constructor

in non-async-await mode, the ctor needs to be able to tell if the first arg is an objid. 
or, at least be able to run with n args or 0 args; 
in the latter case the X() call would be followed up by a setObjectId(). 
NOPE. in the 0-args case we couldn't tell if this need to be 0-args call to native, 
or do nothing, will be followed up by setObjId(). 
we do need to tell these cases apart based on the first param. 

in async-await, we can only have no-args ctor X(), 
becuase we'd need to make a to-native call with the args, and then wait for the result, 
BUt a constructor can't be async. 
so instead, we have a no-args, no-async X() (inits objId as "uninited"), 
and then a spearate init() func. 
and then we can also have a _separate_ initWithObjId(). 



so, is regNewObj(newObj, classname, ctorparmas) sync or async? 
if async => init() needs to be async => classedByName[cn]().init() in export or callback => 
	=> export, callback need to be async .