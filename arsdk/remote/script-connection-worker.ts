
/*
    ScriptConnectionWorker
    *   Starts a websocket server that the remote app can connect to. 
    *   Primarily used by RemoteApp. 
    *   Should be launched as a worker thread. 
    *   Can send and receive (string) messages to / from the remote app. 
    *   Once receives a message, notifies its parent thread about it. 
    *   Actually sends two notifications: 
        *   Atomics.notify(this.messageLengthArray, 0, 1)
        *   parentPort?.postMessage("didReceiveRemoteMessage")
    *   It's up to the parent to decide which notification to listen to. 
        RemoteApp uses them both: 
        Atomics.notify() -> Atomics.wait() while blocked waiting for a call response, 
        postMessage() -> worker.on('message') otherwise. 
        See the comments in remote-app.ts for details. 
*/

import { workerData, parentPort } from 'worker_threads'
import WebSocket from "ws"
import { exec } from "child_process"

export class ScriptConnectionWorker { 
    constructor() { 
        this.messageLengthArray = new Int32Array(workerData.messageLengthBuffer)
        this.messageArray = new Uint8Array(workerData.messageBuffer)
        parentPort?.on('message', (message) => { this.didReceiveMainThreadMessage(message) })
        this.startServer()
    }

    private messageLengthArray: Int32Array
    private messageArray: Uint8Array
    private wsServer?: WebSocket.Server = undefined
    private webSocket?: WebSocket = undefined

    startServer() {  
        this.wsServer = new WebSocket.Server({ port: 2897 })
        this.wsServer.on('connection', (webSocket: WebSocket) => {
            this.webSocket = webSocket
            webSocket.on('message', (message) => { 
                this.didReceiveRemoteMessage(message)
            })
            parentPort?.postMessage("clientDidConnect")
        })

        exec(
            "gp url 2897", 
            (_error, stdout, _stderr) => { 
                let url = stdout
                parentPort?.postMessage("ScriptConnectionWorker: listening at " + url)
            })        

    }

    didReceiveRemoteMessage(remoteMessage: any) { 
        let messagesString: string = remoteMessage.toString()
        let messageStrings = messagesString.split("###arsdk-message-separator###")
        for (const messageString of messageStrings) {
            if (messageString === "") continue

            ;(new TextEncoder()).encodeInto(messageString, this.messageArray)
            let messageStringLength = messageString.length
            this.messageLengthArray[0] = messageStringLength
            Atomics.notify(this.messageLengthArray, 0, 1)

            parentPort?.postMessage("didReceiveRemoteMessage")
            // parentPort?.postMessage(`received, time = ${(new Date()).getTime()}`)

            //  wait for the main thread to set length back to 0 once it finished receiving the message. 
            Atomics.wait(this.messageLengthArray, 0, messageStringLength) 
        }
    }

    didReceiveMainThreadMessage(message: string) { 
        this.sendToNative(message)
    }

    sendToNative(message: string) { 
        // parentPort?.postMessage(`sending, time = ${(new Date()).getTime()}`)
        this.webSocket?.send(message)
    }
}

export let scriptConnectionWorker = new ScriptConnectionWorker()
