

/*
    this is the runtime counterpart to arsdk.d.ts: 
    *   the `import * as arsdk from 'arsdk'` statement (in main.ts) will ultimately, 
        at runtime, load and execute this file. 
    *   basically its job is to make sure that that import statement in main.ts 
        works as expected. 
    *   in other words, make sure that the symbols declared in arsdk.d.ts 
        are indeed available at runtime. 
    *   the way it does so depends on platform (android / ios / remote). 
    *   it knows what platform it runs on by looking at the global arsdk_platform variable. 

    the way this file gets executed: 
    *   main.ts has an `import * from 'arsdk'` statement. 
    *   the ts -> js compilation translates that into 
        `arsdk = __importStar(require('arsdk'))`. 
    *   when running in gitpod, `require()` is the standard node.js `require()`, 
        will load arsdk.js (this file). 
    *   when running on device, 
        *   we use `parcel` to package all source files 
            into a single .js bundle. 
        *   `parcel` replaces `require()` with one that loads the module 
            not from a separate file, but from a local array inside the bundle. 
        *   this is why we can use `import` in main.ts even on device 
            (and don't need to hack-remove the `import` the way we did earlier). 

    providing implementations for arsdk symbols: 
    *   this depends on platform. 
    *   ultimately we need `require('arsdk')` (this is what the `import` statement 
        will be translated to) to return an array with the correct exported symbols. 
    *   to make that happen, we need to put all this exported symbols into 
        an array called `exports` in this file. 
    *   if we do this, then executing `import * as arsdk from 'arsdk'` will result in 
        `arsdk` containing all arsdk symbols. 
    *   android: 
        *   the on-device runtime exposes all arsdk symbols as globals. 
        *   then it loads and executes the on-device bundle. 
        *   the `import` statement in main.ts loads this module (arsdk.js). 
        *   here we need to copy the symbols from the global namespace into `exports`. 
    *   cloud scripting (gitpod): 
        *   execution starts not with main.ts, but with arsdk/remote/remote-bootstrap.ts. 
        *   remote-bootstrap.ts imports remote-app.ts, which implements the so-called 
            "narrow interface", most importantly the `executeNative_str()` function. 
        *   then it loads main.ts. 
        *   the `import` statement in main.ts loads this module (arsdk.js). 
        *   here, in arsdk.js, we `require('arsdk-impl')` (see narrow-intf/arsdk-impl.ts), 
            which defines (not only declares but also implements) all arsdk symbols 
            in terms of that narrow interface. 
        *   now we have all arsdk symbols inside an object (returned from `require()`), 
            we can copy them into `exports`. 
    *   ios: 
        *   similar to the cloud case. 
        *   not really ios specific, could be used on any platform that 
            exposes the narrow interface. 
        *   the on-device runtime only directly exposes a "narrow interface", 
            most importantly the `executeNative_str()` function. 
        *   then it loads main.ts, and the `import` in main.ts loads arsdk.js (this file). 
        *   just like in the cloud case, here we load arsdk-impl, 
            and then copy arsdk symbols to `exports`. 
        *   note arsdk-impl's init() needs to be called. 
            in the cloud case it's done by remote-bootstrap; 
            on ios ????
    *   note arsdk-impl (implements arsdk symbols on top of the narrow interface)
        is always part of the on-mobile-device bundle, even on android, 
        but on android it's largely ignored (we only use it to get a list of symbol 
        names that we need to look for in the global namespace). 

    
        //  outdated comments below

    tries to find implementations of arsdk symbols
    (those also declared in arsdk.d.ts), 
    and then export them in a way that ultimatly
        import * as arsdk from 'arsdk' 
    should work in main.ts. 
    *   if remote scripting, then import them from 'arsdk-remote'. 
    *   if running on device, then check if an `arsdk` global exists; 
        if so, then export everything found inside that 'arsdk' global. 
    *   otherwise, look for arsdk symbols in the global namespace. 

    

    todo
    nope, it's split up in wrong ways. 
    on ios we still need arsdk-impl. 
    the condition is not really remote or not, 
    but a narrow-interface implementation or not. 
    so: 
    *   remote -> narrow intf (import arsdk-impl)
        + an impl of the narrow intf itself based on remote-app. 
    *   ios: narrow intf, 
        the narrow intf implemented by the runtime, exposed in an 'arsdk' global. 
    *   android: wide intf, that is, the whole arsdk intf exposed 
        directly by the runtime (rhino), probably as globals. 

    point is, it's not true that ios and android export the same symbols, 
    jsut in different namespaces (global vs "arsdk"). 
    instead, the ios runtime only exports impl for the narrow intf, 
    and relies on an arsdk -> narrow-intf mapping existing in js. 
    
    but. 
    for that arsdk -> narrow-intf mapping to exist for ios, 
    it must be part of the packaged experience script. 
    and then android needs to ignore it somehow. 
    so, how do we detect if a narrow-intf implementation is availble? 
    => check a global var. 
*/

let platform = global.arsdk_platform

//  re-export arsdk symbols in a platform-agnostic, unified way. 
//  (this practically means adding all arsdk symbols to the `exports` array here.)

let useNarrowIntf = (platform === "remote") || (platform === "ios")

let remote = process.argv.length > 2 && process.argv[2] === "remote"
let arsdkImpl = undefined
if (useNarrowIntf) { 
    arsdkImpl = require('./narrow-intf/arsdk-impl')
} else { 
    //  assume that all arsdk symbols are exposed as globals
    arsdkImpl = {}

    //  todo would be real nice not to have to hardwire symbol names here. 
    //  can we load arsdk-impl on android too, only to get this list? then discard. 
    // let symbolNames = ['X', 'Y', 'ARScript_printer', 'print', 'Globals', 'globals']
    // for (let symbolName of symbolNames) 
    
    //  tmp test
    arsdkImpl = require('./narrow-intf/arsdk-impl')
    for (let symbolName in arsdkImpl)

        arsdkImpl[symbolName] = global[symbolName]
}

for (let key in arsdkImpl) { 
    if (key.indexOf("__") == 0) continue
    exports[key] = arsdkImpl[key] 
}
