
let platform_any: any = (global as any).arsdk_platform
if (platform_any === undefined)
    throw "'arsdk_platform' is undefined"
if (typeof platform_any !== "string")
    throw `'arsdk_platform' must be a string (is ${typeof platform_any})`
let platform = platform_any as string

switch (platform) { 
    case "ios": 
        import("./ios-bootstrap")
        break
    case "android": 
        import("./android-bootstrap")
        break
    default: 
        throw `Unknown value for 'arsdk_platform': ${platform}`
}
