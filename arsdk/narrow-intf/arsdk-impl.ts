
/*
    Implements the ARSDK interface on top of a narrow interface, 
    defined by Requirements. 
    You pass in a suitable implementation of Requirements, 
    you get back working implementations of ARSDK classes and functions
    (the same ones defined in arsdk.d.ts). 
    Used on iOS and in cloud scripting. 
    For Requirements, you most likely will want to use an instance of ARScript
    (and ARScript will need an implementation of executeNative_str). 
    Usage example: 
        import * as arsdk from "../narrow-intf/arsdk-impl"
        import { ARScript } from "../arscript"
        let arscript = new ARScript()
        arsdk.init(arscript)
    See remote-bootstrap.ts for a usage example. 
*/

export interface Requirements { 
    executeNative(objectId: string, methodName: string, ...params: any[]): any
    replicateScriptToNativeChecked(newObject: any, className: string, ...params: any[]): void
    replicateScriptToNativeUnchecked(newObject: any, className: string, ...params: any[]): void
    classesByName: { [name: string]: any }
}
var requirements_: Requirements

var classesByName: { [name: string]: any } = []

export function init(requirements: Requirements) { 
    requirements_ = requirements
    requirements_.classesByName = []
    for (let className in classesByName) { 
        let class_ = classesByName[className]
        requirements_.classesByName[className] = class_
    }
}

//  --- 

//  test interface

/*
export class MyCodable { 
	x = 0
	y = 0
}
classesByName['MyCodable'] = MyCodable

export class Y  {
    objectId: string
    constructor(i_orObjectId: number | string) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateScriptToNativeChecked(this, "Y", i_orObjectId) 
    }
	i(): number { return requirements_.executeNative(this.objectId, "i") }
	yfoo(): string { return requirements_.executeNative(this.objectId, "yfoo") }
	set_i(newValue: number) { return requirements_.executeNative(this.objectId, "set_i", newValue) }
	takesCodable(c: MyCodable) { return requirements_.executeNative(this.objectId, "takesCodable", c) }
}
classesByName['Y'] = Y

export class X {
	objectId: string
	static staticFoo(): string { return requirements_.executeNative("arsdk_class_X", "staticFoo") }
    static staticWithParam(p0: number): number { return requirements_.executeNative("arsdk_class_X", "staticWithParam", p0) }
    
    //  todo would be nice to enable this getter / setter syntax
	// get i(): number { return requirements_.executeNative(this.objectId, "get__i") }
	// set i(newValue: number) { requirements_.executeNative(this.objectId, "set__i", newValue) }
	get_i(): number { return requirements_.executeNative(this.objectId, "get__i") }
	set_i(newValue: number) { requirements_.executeNative(this.objectId, "set__i", newValue) }
	get readOnly(): number { return requirements_.executeNative(this.objectId, "get__readOnly") } 

    constructor(maybeObjectId: string | undefined) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateScriptToNativeChecked(this, "X", maybeObjectId) 
    }
	
    callCallback2() { return requirements_.executeNative(this.objectId, "callCallback2") }
	setCallback2(callback: (x: X) => void) { return requirements_.executeNative(this.objectId, "setCallback2", callback) }
	thrower() { return requirements_.executeNative(this.objectId, "thrower") }
	setCallback(callback: (n: number) => void) { return requirements_.executeNative(this.objectId, "setCallback", callback) }
	returnString(): string { return requirements_.executeNative(this.objectId, "returnString") }
	fp2(p0: number, p1: number): number { return requirements_.executeNative(this.objectId, "fp2", p0, p1) }
	returnInt(): number { return requirements_.executeNative(this.objectId, "returnInt") }
	foo() { return requirements_.executeNative(this.objectId, "foo") }
	callCallback() { return requirements_.executeNative(this.objectId, "callCallback") }
	createY(): Y { return requirements_.executeNative(this.objectId, "createY") }
	takesOptional(p0: number | null) { return requirements_.executeNative(this.objectId, "takesOptional", p0) }
}
classesByName['X'] = X

export class ARScript_Printer {
	objectId: string
	static print(p0: string) { return requirements_.executeNative("arsdk_class_ARScript_Printer", "print", p0) }
    constructor(maybeObjectId: string | undefined) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateScriptToNativeChecked(this, "ARScript_Printer", maybeObjectId) 
    }
}
classesByName['ARScript_Printer'] = ARScript_Printer

export function print(str: any) {
	console.log(str.toString())
	ARScript_Printer.print(str.toString())
}

//  globals
let nativeX: X

*/

//  --- ARSDK ---

class ARScript_Printer {
    objectId: string
	static print(p0: string) { return requirements_.executeNative("arsdk_class_ARScript_Printer", "print", p0) }
    constructor(maybeObjectId: string | undefined) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateScriptToNativeChecked(this, "ARScript_Printer", maybeObjectId) 
    }    
}
classesByName["ARScript_Printer"] = ARScript_Printer

class ObjectInstance {
    objectId: string
	get position() { return requirements_.executeNative(this.objectId, "get__position") }
	set position(newValue) { requirements_.executeNative(this.objectId, "set__position", newValue) }
    constructor(maybeObjectId: string | undefined) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateScriptToNativeChecked(this, "ObjectInstance", maybeObjectId) 
    }    
	setScale(p0: number[]) { return requirements_.executeNative(this.objectId, "setScale", p0) }
	setOpacity(p0: number) { return requirements_.executeNative(this.objectId, "setOpacity", p0) }
	setPosition(p0: number[]) { return requirements_.executeNative(this.objectId, "setPosition", p0) }
	whenSelected(p0: (instance: ObjectInstance) => void) { return requirements_.executeNative(this.objectId, "whenSelected", p0) }
	whenTapped(p0: (instance: ObjectInstance) => void) { return requirements_.executeNative(this.objectId, "whenTapped", p0) }
	whenPlaced(p0: (instance: ObjectInstance) => void) { return requirements_.executeNative(this.objectId, "whenPlaced", p0) }
	stopAllAnimations() { return requirements_.executeNative(this.objectId, "stopAllAnimations") }
	whenDeleted(p0: (instance: ObjectInstance) => void) { return requirements_.executeNative(this.objectId, "whenDeleted", p0) }
	getPosition(): number[] { return requirements_.executeNative(this.objectId, "getPosition") }
	playAnimation(p0: string) { return requirements_.executeNative(this.objectId, "playAnimation", p0) }
    getAnimationNames(): string[] { return requirements_.executeNative(this.objectId, "getAnimationNames") }
}
classesByName["ObjectInstance"] = ObjectInstance

class ScriptExperience {
    objectId: string
    constructor(maybeObjectId: string | undefined) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateScriptToNativeChecked(this, "ScriptExperience", maybeObjectId) 
    }    
	getObjectInstance(p0: string): ObjectInstance { return requirements_.executeNative(this.objectId, "getObjectInstance", p0) }
	setInterval(p0: number, p1: () => void) { return requirements_.executeNative(this.objectId, "setInterval", p0, p1) }
	getCameraPosition(): number[] { return requirements_.executeNative(this.objectId, "getCameraPosition") }
    getCameraAngles(): number[] { return requirements_.executeNative(this.objectId, "getCameraAngles") }
    getObjectDefinitionNames(): string[] { return requirements_.executeNative(this.objectId, "getObjectDefinitionNames") }
	getObjectDefinition(p0: string): ObjectDefinition { return requirements_.executeNative(this.objectId, "getObjectDefinition", p0) }
	getPlanePosition(): number[] { return requirements_.executeNative(this.objectId, "getPlanePosition") }
}
classesByName["ScriptExperience"] = ScriptExperience

class ObjectDefinition {
    objectId: string
    constructor(maybeObjectId: string | undefined) { 
        this.objectId = "arsdk_uninitialized_objectId"
        requirements_.replicateScriptToNativeChecked(this, "ObjectDefinition", maybeObjectId) 
    }    
	whenInstanceCreated(p0: (instance: ObjectInstance) => void) { return requirements_.executeNative(this.objectId, "whenInstanceCreated", p0) }
}
classesByName["ObjectDefinition"] = ObjectDefinition

export function print(str: any) {
    console.log(str.toString())
    ARScript_Printer.print(str.toString())
}

export let experience: ScriptExperience
