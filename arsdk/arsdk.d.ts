

//  --- test interface ---

/*

export declare class MyCodable { 
	x: number
	y: number
}

export declare class Y  {
    objectId: string
    constructor(i: number)
    i(): number
    yfoo(): string
    set_i(newValue: number): void
    takesCodable(c: MyCodable): void
}

export declare class X {
    static staticFoo(): string
    static staticWithParam(p0: number): number
    //  todo would be nice to enable this getter / setter syntax
    // get i(): number
    // set i(newValue: number)
    get_i(): number
    set_i(newValue: number): void
    readOnly(): number
    	
    callCallback2(): void
    setCallback2(callback: (x: X) => void): void
    thrower(): void
    setCallback(callback: (n: number) => void): void
    returnString(): string
    fp2(p0: number, p1: number): number
    returnInt(): number
    foo(): void
    callCallback(): void
    createY(): Y
    takesOptional(p0: number | null): void
}

export declare function print(str: any): void 

export let nativeX: X

*/

//  --- ARSDK interface ---

export interface ObjectInstance {
	// get position()
	// set position(newValue)
    setScale(p0: number[]): void 
    setOpacity(p0: number): void
    setPosition(p0: number[]): void
    whenSelected(p0: (instance: ObjectInstance) => void): void
    whenTapped(p0: (instance: ObjectInstance) => void): void
    whenPlaced(p0: (instance: ObjectInstance) => void): void
    stopAllAnimations(): void
    whenDeleted(p0: (instance: ObjectInstance) => void): void
    getPosition(): number[]
    playAnimation(p0: string): void
    getAnimationNames(): string[]
}

export interface ScriptExperience {
    getObjectInstance(p0: string): ObjectInstance
    setInterval(p0: number, p1: () => void): void
    getCameraPosition(): number[] 
    getCameraAngles(): number[] 
    getObjectDefinitionNames(): string[]
    getObjectDefinition(p0: string): ObjectDefinition 
    getPlanePosition(): number[] 
}

export interface ObjectDefinition {
    whenInstanceCreated(p0: (instance: ObjectInstance) => void): void
}

export function print(str: any): void

export var experience: ScriptExperience